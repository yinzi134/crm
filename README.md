 云帆CRM系统 开源版

# 项目演示
商业版本：https://doc.yfhl.net/

# 介绍
一款多角色客户管理系统，系统集成了用户管理、角色管理、部门管理、客户公海、保留客户、客户动态，客户历史。对客户的跟进过程进行记录和管理。

# 技术栈
SpringBoot / Redis / Shiro / Vue / MySQL

# 产品功能  
权限控制：基于Shiro和JWT开发的权限控制功能。

用户系统：用户管理、部门管理、角色管理等。

客户公海：保留客户中的客户资料释放之后会进入客户公海，由其他的销售人员进行认领后跟进。

保留客户：对客户信息，联系人等基本信息进行记录和管理，对客户跟进过程进行记录和管理。

客户动态：客户的跟进过程的动态展示。

客户历史：释放或者放弃的客户的信息的展示。

# 环境要求
JDK 1.8+  [点此下载](https://cdn.yfhl.net/java-win/jdk-8u181-windows-x64.exe)        
Mysql5.7+  [点此下载](https://cdn.yfhl.net/java-win/mysql-installer-community-5.7.31.0.msi)  
Redis3.2+  [点此下载](https://cdn.yfhl.net/java-win/Redis-x64-3.2.100.msi)


# 快速运行
1、自行安装MySQL数据库（版本最好大于5.7），将安装资源中的yf_crm.sql导入到安装好的数据库

2、安装Java环境，要求JDK版本大于1.7

3、请修改外置配置文件：application-local.yml 改成您自己的MySQL配置。 

4、Windows通过start.bat运行，Linux运行start.sh运行

5、如果无意外，可通过：http://localhost:8210 访问到项目了 

6、管理员账号密码：admin/admin

# 其它支持
QQ交流群：537163131      
手机：18603038204(郭经理)   
手机：18710213152(杨经理)    
网站：https://www.jeedocm.com/?plan=oscrmdora