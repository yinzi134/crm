import defaultSettings from '@/settings'
import { fetchDetail } from '@/api/sys/config/base'

const { tagsView, fixedHeader, sidebarLogo } = defaultSettings

const state = {
  tagsView: tagsView,
  fixedHeader: fixedHeader,
  sidebarLogo: sidebarLogo,
  siteData: {}
}

// 网站数据
const siteCacheKey = 'ycloud_site_cache'

const mutations = {

  SET_SITE_DATA: (state, siteData) => {
    state.siteData = siteData
  }
}

const actions = {
  // 获取网站配置信息
  getSite({ commit }) {
    return new Promise((resolve, reject) => {
      // 获取缓存信息
      const json = localStorage.getItem(siteCacheKey)

      if (json != null && json != '') {
        const data = JSON.parse(json)
        commit('SET_SITE_DATA', data)
        resolve(data)
      } else {
        // 重新获取
        fetchDetail({}).then(response => {
          const { data } = response
          localStorage.setItem(siteCacheKey, JSON.stringify(data))
          commit('SET_SITE_DATA', data)
          resolve(data)
        }).catch(error => {
          reject(error)
        })
      }
    })
  },
  delSiteCache({ dispatch, state }, view) {
    return new Promise(() => {
      localStorage.removeItem(siteCacheKey)
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}

