import { login, mobileLogin, reg, logout } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
import { resetRouter } from '@/router'

const state = {
  token: getToken(),
  userId: '',
  name: '',
  realName: '',
  avatar: '',
  roleType: '',
  roles: [],
  permissions: []
}

// 用户缓存Key
const userCacheKey = 'ycloud_user_cache'

const mutations = {
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_ROLE_TYPE: (state, roleType) => {
    state.roleType = roleType
  },
  SET_ID: (state, userId) => {
    state.userId = userId
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_REAL_NAME: (state, realName) => {
    state.realName = realName
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  SET_ROLES: (state, roles) => {
    state.roles = roles
  },
  SET_PERMISSIONS: (state, permissions) => {
    state.permissions = permissions
  }
}

const actions = {

  login({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      login(userInfo).then(response => {
        const { data } = response
        // 登录后缓存到本地
        localStorage.setItem(userCacheKey, JSON.stringify(data))
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  mobileLogin({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      mobileLogin(userInfo).then(response => {
        const { data } = response
        // 登录后缓存到本地
        localStorage.setItem(userCacheKey, JSON.stringify(data))
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve(data)
      }).catch(error => {
        reject(error)
      })
    })
  },

  reg({ commit }, userInfo) {
    return new Promise((resolve, reject) => {
      reg(userInfo).then(response => {
        const { data } = response
        localStorage.setItem(userCacheKey, JSON.stringify(data))
        commit('SET_TOKEN', data.token)
        setToken(data.token)
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 获取用户信息
  getInfo({ commit, state }) {
    return new Promise((resolve, reject) => {
      // 获取缓存中的用户
      const json = localStorage.getItem(userCacheKey)
      if (json == null || json == '') {
        reject('会话超时，请重新登录！')
      } else {
        const data = JSON.parse(json)
        commit('SET_ID', data.id)
        commit('SET_ROLES', data.roles)
        commit('SET_REAL_NAME', data.realName)
        commit('SET_NAME', data.userName)
        commit('SET_AVATAR', data.avatar)
        commit('SET_ROLE_TYPE', data.roleType)
        commit('SET_PERMISSIONS', data.permissions)
        resolve(data)
      }
    })
  },

  // 退出登录
  logout({ commit, state, dispatch }) {
    return new Promise((resolve, reject) => {
      logout(state.token).then(() => {
        commit('SET_TOKEN', '')
        commit('SET_ROLES', [])
        removeToken()
        resetRouter()
        // 清理用户
        localStorage.removeItem(userCacheKey)
        // 清理网站缓存
        dispatch('settings/delSiteCache', null, { root: true })
        dispatch('tagsView/delAllViews', null, { root: true })
        resolve()
      }).catch(error => {
        reject(error)
      })
    })
  },

  // 移除会话
  resetToken({ commit, dispatch }) {
    return new Promise(resolve => {
      commit('SET_TOKEN', '')
      commit('SET_ROLES', [])
      // 清理用户
      localStorage.removeItem(userCacheKey)
      // 清理网站缓存
      dispatch('settings/delSiteCache', null, { root: true })
      removeToken()
      resolve()
    })
  }
}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
