import { post } from '@/utils/request'

/**
 * 发送短信验证码
 * @param id
 * @returns {*}
 */
export function send(data) {
  return post('/api/ability/sms/send', data)
}
