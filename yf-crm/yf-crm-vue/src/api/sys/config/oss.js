import { post } from '@/utils/request'

export function fetchToken() {
  return post('/api/ability/oss/token')
}
