import { post } from '@/utils/request'

export function apiDynamicPaging(data) {
  return post('/api/customer/dynamic/paging', data)
}

