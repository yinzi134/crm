import { post } from '@/utils/request'

export function apiSaveCustomer(data) {
  return post('/api/customer/customer/save', data)
}

export function apiCustomerDetail(id) {
  return post('/api/customer/customer/detail', { id: id })
}

// 认领客户
export function apiCustomerGot(ids) {
  return post('/api/customer/customer/got', { ids: ids })
}

// 释放客户
export function apiCustomerRelease(data) {
  return post('/api/customer/customer/release', data)
}

