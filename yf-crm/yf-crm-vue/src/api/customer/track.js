import { post } from '@/utils/request'

export function apiSaveContactRecord(data) {
  return post('/api/customer/contact/save', data)
}

export function apiContactRecordDetail(id) {
  return post('/api/customer/contact/detail', { id: id })
}
