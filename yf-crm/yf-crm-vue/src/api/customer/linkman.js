import { post } from '@/utils/request'

export function apiSaveContacts(data) {
  return post('/api/customer/linkman/save', data)
}

export function apiContactsDetail(id) {
  return post('/api/customer/linkman/detail', { id: id })
}

export function apiContactsPaging(data) {
  return post('/api/customer/linkman/paging', data)
}

export function apiMarkMain(data) {
  return post('/api/customer/linkman/mark', data)
}
