import { post } from '@/utils/request'

export function apiCustomerStatPie(data) {
  return post('/api/customer/stat/level-pie', data)
}

export function apiCustomerStatTotal(data) {
  return post('/api/customer/stat/total-stat', data)
}


