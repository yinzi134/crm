package com.yf.modules.customer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import java.util.Date;

/**
* <p>
* 客户修改日志实体类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@TableName("cs_log")
public class CsLog extends Model<CsLog> {

    private static final long serialVersionUID = 1L;

    /**
    * ID
    */
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    /**
    * 客户ID
    */
    @TableField("customer_id")
    private String customerId;

    /**
    * 修改时间
    */
    @TableField("update_time")
    private Date updateTime;

    /**
    * 修改人
    */
    @TableField("update_by")
    private String updateBy;

    /**
    * 旧JSON数据
    */
    @TableField("old_data")
    private String oldData;

    /**
    * 新JSON数据
    */
    @TableField("new_data")
    private String newData;

}
