package com.yf.modules.customer.dto;

import com.yf.boot.base.api.annon.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
* <p>
* 联系记录数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="联系记录", description="联系记录")
public class CsContactDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "ID", required=true)
    private String id;

    @Dict(dicCode = "contact_type")
    @ApiModelProperty(value = "联系方法")
    private String contactType;

    @Dict(dictTable = "cs_customer", dicCode = "id", dicText = "name")
    @ApiModelProperty(value = "客户ID")
    private String customerId;

    @Dict(dictTable = "cs_linkman", dicCode = "id", dicText = "name")
    @ApiModelProperty(value = "联系人ID")
    private String contactId;

    @ApiModelProperty(value = "联系时间")
    private Date createTime;

    @Dict(dictTable = "sys_user", dicCode = "id", dicText = "real_name")
    @ApiModelProperty(value = "联系人")
    private String createBy;

    @Dict(dicCode = "contact_result")
    @ApiModelProperty(value = "联系结果")
    private String result;

    @ApiModelProperty(value = "备注信息")
    private String remark;

}
