package com.yf.modules.customer.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsCustomerDTO;
import com.yf.modules.customer.dto.request.CustomerSaveReqDTO;
import com.yf.modules.customer.entity.CsCustomer;

import java.util.List;

/**
* <p>
* 客户信息业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsCustomerService extends IService<CsCustomer> {

    /**
    * 公海客户
    * @param reqDTO
    * @return
    */
    IPage<CsCustomerDTO> pagingSea(PagingReqDTO<CsCustomerDTO> reqDTO);

    /**
     * 保留客户
     * @param reqDTO
     * @return
     */
    IPage<CsCustomerDTO> pagingKeep(PagingReqDTO<CsCustomerDTO> reqDTO);


    /**
     * 批量认领客户
     * @param userId
     * @param ids
     */
    void got(String userId, List<String> ids);

    /**
     * 释放客户
     * @param userId
     * @param result
     * @param remark
     * @param ids
     */
    void release(String userId, String result, String remark, List<String> ids);

    /**
     * 更新最近联系时间
     * @param id
     * @param userId
     */
    void updateContact(String id, String userId);

    /**
     * 添加客户
     * @param reqDTO
     */
    void save(CustomerSaveReqDTO reqDTO);
}
