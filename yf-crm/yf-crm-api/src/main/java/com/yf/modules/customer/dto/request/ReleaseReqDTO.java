package com.yf.modules.customer.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
* <p>
* 客户信息数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="释放客户请求类", description="释放客户请求类")
public class ReleaseReqDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "客户ID", required=true)
    private List<String> ids;

    @ApiModelProperty(value = "结果")
    private String result;

    @ApiModelProperty(value = "备注信息")
    private String remark;

}
