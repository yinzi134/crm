package com.yf.modules.customer.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsLinkmanDTO;
import com.yf.modules.customer.entity.CsLinkman;

/**
* <p>
* 联系人业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsLinkmanService extends IService<CsLinkman> {

    /**
    * 分页查询数据
    * @param reqDTO
    * @return
    */
    IPage<CsLinkmanDTO> paging(PagingReqDTO<CsLinkmanDTO> reqDTO);

    /**
     * 标记为主要联系人
     * @param id
     */
    void markMain(String id);
}
