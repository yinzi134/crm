package com.yf.modules.sys.config.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.utils.BeanMapper;
import com.yf.modules.sys.config.dto.CfgUploadQiniuDTO;
import com.yf.modules.sys.config.entity.CfgUploadQiniu;
import com.yf.modules.sys.config.mapper.CfgUploadQiniuMapper;
import com.yf.modules.sys.config.service.CfgUploadQiniuService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
* <p>
* 七牛云上传配置业务实现类
* </p>
*
* @author 聪明笨狗
* @since 2021-02-24 19:21
*/
@Service
public class CfgUploadQiniuServiceImpl extends ServiceImpl<CfgUploadQiniuMapper, CfgUploadQiniu> implements CfgUploadQiniuService {

    @Override
    public void save(CfgUploadQiniuDTO reqDTO) {
        CfgUploadQiniu entity = new CfgUploadQiniu();
        BeanMapper.copy(reqDTO, entity);
        this.saveOrUpdate(entity);
    }

    @Override
    public CfgUploadQiniuDTO find() {
        List<CfgUploadQiniu> list =  this.list();
        CfgUploadQiniuDTO respDTO = new CfgUploadQiniuDTO();
        if(!CollectionUtils.isEmpty(list)){
            BeanMapper.copy(list.get(0), respDTO);
        }
        return respDTO;
    }
}
