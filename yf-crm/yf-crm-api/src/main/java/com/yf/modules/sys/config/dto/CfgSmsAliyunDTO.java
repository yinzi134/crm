package com.yf.modules.sys.config.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
* <p>
* 阿里大鱼短信数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-03-30 18:17
*/
@Data
@ApiModel(value="阿里大鱼短信", description="阿里大鱼短信")
public class CfgSmsAliyunDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "ID", required=true)
    private String id;

    @ApiModelProperty(value = "秘钥ID")
    private String accessKeyId;

    @ApiModelProperty(value = "秘钥密码")
    private String accessKeySecret;

    @ApiModelProperty(value = "短信签名")
    private String sign;

    @ApiModelProperty(value = "模板ID")
    private String tmpl;

}
