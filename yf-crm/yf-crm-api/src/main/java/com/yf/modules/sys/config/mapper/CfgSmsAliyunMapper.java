package com.yf.modules.sys.config.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.modules.sys.config.entity.CfgSmsAliyun;
/**
* <p>
* 阿里大鱼短信Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-03-30 18:17
*/
public interface CfgSmsAliyunMapper extends BaseMapper<CfgSmsAliyun> {

}
