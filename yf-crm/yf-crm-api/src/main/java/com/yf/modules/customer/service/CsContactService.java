package com.yf.modules.customer.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsContactDTO;
import com.yf.modules.customer.entity.CsContact;

/**
* <p>
* 联系记录业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsContactService extends IService<CsContact> {

    /**
    * 分页查询数据
    * @param reqDTO
    * @return
    */
    IPage<CsContactDTO> paging(PagingReqDTO<CsContactDTO> reqDTO);

    /**
     * 保存记录
     * @param reqDTO
     */
    void save(CsContactDTO reqDTO);
}
