package com.yf.modules.stat.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
* <p>
* 客户跟进历史数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="总体统计信息", description="总体统计信息")
public class CsTotalStatDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "总客户数")
    private Integer totalNum;

    @ApiModelProperty(value = "本月新增客户")
    private Integer monthNewNum;

    @ApiModelProperty(value = "本月新增客户")
    private Integer monthContactNum;

}
