package com.yf.modules.customer.dto;

import com.yf.boot.base.api.annon.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* <p>
* 客户跟进历史数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="客户跟进历史", description="客户跟进历史")
public class CsTrackDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "ID", required=true)
    private String id;

    @Dict(dictTable = "cs_customer", dicCode = "id", dicText = "name")
    @ApiModelProperty(value = "客户ID")
    private String customerId;

    @Dict(dictTable = "sys_user", dicCode = "id", dicText = "real_name")
    @ApiModelProperty(value = "用户ID")
    private String createBy;

    @ApiModelProperty(value = "部门ID")
    private String deptCode;

    @ApiModelProperty(value = "开始跟进日期")
    private Date startTime;

    @ApiModelProperty(value = "结束跟进日期")
    private Date endTime;

    @Dict(dicCode = "customer_track")
    @ApiModelProperty(value = "跟进结果")
    private String result;

    @ApiModelProperty(value = "正在跟进")
    private Boolean isCurrent;

    @ApiModelProperty(value = "备注信息")
    private String remark;


    /**
     * 客户持有天数
     * @return
     */
    @ApiModelProperty(value = "备注信息")
    public Integer getKeepDay(){
        if(this.startTime == null || this.endTime==null){
            return 0;
        }

        if(this.startTime.after(this.endTime)){
            return 0;
        }

       return  (int)((endTime.getTime() - startTime.getTime()) / 1000 / 60 / 60 / 24);
    }

}
