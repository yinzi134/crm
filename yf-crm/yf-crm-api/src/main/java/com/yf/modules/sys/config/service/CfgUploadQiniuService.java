package com.yf.modules.sys.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.modules.sys.config.dto.CfgUploadQiniuDTO;
import com.yf.modules.sys.config.entity.CfgUploadQiniu;

/**
* <p>
* 七牛云上传配置业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-02-24 19:21
*/
public interface CfgUploadQiniuService extends IService<CfgUploadQiniu> {

    /**
     * 保存配置
     * @param reqDTO
     */
    void save(CfgUploadQiniuDTO reqDTO);

    /**
     * 查找配置
     * @return
     */
    CfgUploadQiniuDTO find();
}
