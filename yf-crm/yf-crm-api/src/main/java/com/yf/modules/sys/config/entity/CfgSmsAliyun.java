package com.yf.modules.sys.config.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
* <p>
* 阿里大鱼短信实体类
* </p>
*
* @author 聪明笨狗
* @since 2021-03-30 18:17
*/
@Data
@TableName("el_cfg_sms_aliyun")
public class CfgSmsAliyun extends Model<CfgSmsAliyun> {

    private static final long serialVersionUID = 1L;

    /**
    * ID
    */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
    * 秘钥ID
    */
    @TableField("access_key_id")
    private String accessKeyId;

    /**
    * 秘钥密码
    */
    @TableField("access_key_secret")
    private String accessKeySecret;

    /**
    * 短信签名
    */
    private String sign;

    /**
     * 模板ID
     */
    private String tmpl;

}
