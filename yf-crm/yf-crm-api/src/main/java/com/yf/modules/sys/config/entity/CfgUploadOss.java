package com.yf.modules.sys.config.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;

/**
* <p>
* 阿里云上传配置实体类
* </p>
*
* @author 聪明笨狗
* @since 2021-02-05 11:16
*/
@Data
@TableName("el_cfg_upload_oss")
public class CfgUploadOss extends Model<CfgUploadOss> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.ASSIGN_ID)
    private String id;

    /**
     * 存储Bucket
     */
    private String bucket;

    /**
     * 秘钥ID
     */
    @TableField("access_key_id")
    private String accessKeyId;

    /**
     * 秘钥密码
     */
    @TableField("access_key_secret")
    private String accessKeySecret;

    /**
     * 节点
     */
    private String endpoint;

    /**
     * 访问路径
     */
    private String url;

    /**
     * 智能媒体项目名
     */
    private String project;

    /**
     * MPS管道ID
     */
    private String pipeline;

}
