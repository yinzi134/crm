package com.yf.modules.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.modules.customer.entity.CsContact;
/**
* <p>
* 联系记录Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsContactMapper extends BaseMapper<CsContact> {

}
