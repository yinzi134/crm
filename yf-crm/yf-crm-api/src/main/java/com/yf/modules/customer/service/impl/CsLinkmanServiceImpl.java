package com.yf.modules.customer.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsLinkmanDTO;
import com.yf.modules.customer.entity.CsLinkman;
import com.yf.modules.customer.mapper.CsLinkmanMapper;
import com.yf.modules.customer.service.CsLinkmanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 * 联系人业务实现类
 * </p>
 *
 * @author 聪明笨狗
 * @since 2021-07-07 12:01
 */
@Service
public class CsLinkmanServiceImpl extends ServiceImpl<CsLinkmanMapper, CsLinkman> implements CsLinkmanService {

    @Override
    public IPage<CsLinkmanDTO> paging(PagingReqDTO<CsLinkmanDTO> reqDTO) {

        // 创建分页对象
        Page query = new Page(reqDTO.getCurrent(), reqDTO.getSize());


        //查询条件
        QueryWrapper<CsLinkman> wrapper = new QueryWrapper<>();

        // 请求参数
        CsLinkmanDTO params = reqDTO.getParams();

        // 倒序排列
        wrapper.lambda()
                .eq(CsLinkman::getCustomerId, params.getCustomerId())
                .orderByDesc(CsLinkman::getCreateTime);


        //获得数据
        IPage<CsLinkman> page = this.page(query, wrapper);
        //转换结果
        IPage<CsLinkmanDTO> pageData = JSON.parseObject(JSON.toJSONString(page), new TypeReference<Page<CsLinkmanDTO>>() {
        });
        return pageData;
    }


    @Transactional(rollbackFor = Exception.class)
    @Override
    public void markMain(String id) {

        CsLinkman contacts = this.getById(id);
        QueryWrapper<CsLinkman> wrapper = new QueryWrapper<>();
        wrapper.lambda().eq(CsLinkman::getCustomerId, contacts.getCustomerId());

        CsLinkman empty = new CsLinkman();
        empty.setIsMain(false);
        this.update(empty, wrapper);

        contacts.setIsMain(true);
        this.updateById(contacts);

    }
}
