package com.yf.modules.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.modules.customer.dto.CsCustomerDTO;
import com.yf.modules.customer.dto.CsTrackDTO;
import com.yf.modules.customer.entity.CsTrack;
import org.apache.ibatis.annotations.Param;

/**
* <p>
* 客户跟进历史Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsTrackMapper extends BaseMapper<CsTrack> {

    /**
     * 查找追踪历史
     * @param page
     * @param reqDTO
     * @return
     */
    IPage<CsTrackDTO> pagingHistory(Page page, @Param("query") CsCustomerDTO reqDTO);
}
