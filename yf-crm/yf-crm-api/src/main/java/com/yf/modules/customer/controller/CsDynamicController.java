package com.yf.modules.customer.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.boot.base.api.api.dto.BaseIdReqDTO;
import com.yf.boot.base.api.api.dto.BaseIdsReqDTO;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsDynamicDTO;
import com.yf.modules.customer.entity.CsDynamic;
import com.yf.modules.customer.service.CsDynamicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
* <p>
* 客户动态控制器
* </p>
*
* @author 聪明笨狗
* @since 2021-07-09 16:16
*/
@Api(tags={"客户动态"})
@RestController
@RequestMapping("/api/customer/dynamic")
public class CsDynamicController extends BaseController {

    @Autowired
    private CsDynamicService baseService;


    /**
    * 批量删除
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "批量删除")
    @RequestMapping(value = "/delete", method = { RequestMethod.POST})
    public ApiRest edit(@RequestBody BaseIdsReqDTO reqDTO) {
        //根据ID删除
        baseService.removeByIds(reqDTO.getIds());
        return super.success();
    }

    /**
    * 查找详情
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/detail", method = { RequestMethod.POST})
    public ApiRest<CsDynamicDTO> find(@RequestBody BaseIdReqDTO reqDTO) {
        CsDynamic entity = baseService.getById(reqDTO.getId());
        CsDynamicDTO dto = new CsDynamicDTO();
        BeanUtils.copyProperties(entity, dto);
        return super.success(dto);
    }

    /**
    * 分页查找
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "分页查找")
    @RequestMapping(value = "/paging", method = { RequestMethod.POST})
    public ApiRest<IPage<CsDynamicDTO>> paging(@RequestBody PagingReqDTO<CsDynamicDTO> reqDTO) {
        //分页查询并转换
        IPage<CsDynamicDTO> page = baseService.paging(reqDTO);
        return super.success(page);
    }
}
