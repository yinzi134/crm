package com.yf.modules.customer.dto.request;

import com.yf.modules.customer.dto.CsLinkmanDTO;
import com.yf.modules.customer.dto.CsCustomerDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
* <p>
* 客户信息数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="客户添加请求类", description="客户添加请求类")
public class CustomerSaveReqDTO extends CsCustomerDTO {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "主要联系人")
    private CsLinkmanDTO contacts;
}
