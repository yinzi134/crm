package com.yf.modules.customer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import java.util.Date;

/**
* <p>
* 联系人实体类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@TableName("cs_linkman")
public class CsLinkman extends Model<CsLinkman> {

    private static final long serialVersionUID = 1L;

    /**
    * ID
    */
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 客户ID
     */
    @TableField("customer_id")
    private String customerId;

    /**
    * 姓名
    */
    private String name;

    /**
    * 职位
    */
    private String position;

    /**
    * 手机号码
    */
    private String mobile;

    /**
    * 邮箱
    */
    private String email;

    /**
    * 微信号码
    */
    private String wechat;

    /**
    * 备注信息
    */
    private String remark;

    /**
    * 主要联系人
    */
    @TableField("is_main")
    private Boolean isMain;

    /**
    * 创建时间
    */
    @TableField("create_time")
    private Date createTime;

    /**
    * 创建人
    */
    @TableField("create_by")
    private String createBy;

}
