package com.yf.modules.customer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import java.util.Date;

/**
* <p>
* 客户信息实体类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@TableName("cs_customer")
public class CsCustomer extends Model<CsCustomer> {

    private static final long serialVersionUID = 1L;

    /**
    * 客户ID
    */
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    /**
    * 客户类型,企业/个人
    */
    @TableField("cus_type")
    private String cusType;

    /**
    * 客户名称
    */
    private String name;

    /**
    * 备注信息
    */
    private String remark;

    /**
    * 联系人
    */
    private String contact;

    /**
    * 手机号
    */
    private String mobile;

    /**
    * 微信号
    */
    private String wechat;

    /**
    * 电子邮箱
    */
    private String email;

    /**
    * 联系地址
    */
    private String address;

    /**
    * 是否公海
    */
    @TableField("is_public")
    private Boolean isPublic;

    /**
    * 客户行业
    */
    private String industry;

    /**
    * 主营产品
    */
    private String product;

    /**
    * 客户级别
    */
    private String level;

    /**
    * 客户来源
    */
    private String source;

    /**
    * 创建时间
    */
    @TableField("create_time")
    private Date createTime;

    /**
    * 创建人
    */
    @TableField("create_by")
    private String createBy;

    /**
    * 更新时间
    */
    @TableField("update_time")
    private Date updateTime;

    /**
    * 更新时间
    */
    @TableField("update_by")
    private String updateBy;

    /**
    * 最近联系时间
    */
    @TableField("contact_time")
    private Date contactTime;

    /**
    * 当前归属部门
    */
    @TableField("dept_code")
    private String deptCode;

    /**
    * 归属人ID
    */
    @TableField("owner_id")
    private String ownerId;

    /**
    * 客户状态
    */
    private String state;

}
