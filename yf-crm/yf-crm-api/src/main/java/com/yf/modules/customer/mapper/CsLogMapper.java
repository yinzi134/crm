package com.yf.modules.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.modules.customer.entity.CsLog;
/**
* <p>
* 客户修改日志Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsLogMapper extends BaseMapper<CsLog> {

}
