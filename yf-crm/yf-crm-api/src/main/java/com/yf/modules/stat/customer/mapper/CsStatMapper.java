package com.yf.modules.stat.customer.mapper;

import com.yf.modules.stat.customer.dto.CsLevelPieStatDTO;
import com.yf.modules.stat.customer.dto.CsTotalStatDTO;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
* <p>
* 联系记录Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Mapper
public interface CsStatMapper {

    /**
     * 客户等级统计
     * @return
     */
    List<CsLevelPieStatDTO> findLevelPie();


    /**
     * 查找基础统计
     * @return
     */
    CsTotalStatDTO findTotal();
}
