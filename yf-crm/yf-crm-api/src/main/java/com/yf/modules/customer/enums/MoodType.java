package com.yf.modules.customer.enums;

/**
 * 心情类型
 * @author bool
 */
public interface MoodType {

    /**
     * 开心
     */
    Integer HAPPY = 1;

    /**
     * 一般
     */
    Integer NORMAL = 2;

    /**
     * 难过
     */
    Integer SAD = 3;

    /**
     * 签约
     */
    Integer SIGN = 4;
}
