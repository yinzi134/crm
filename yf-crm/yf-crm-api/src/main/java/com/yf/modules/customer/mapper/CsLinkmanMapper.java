package com.yf.modules.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.modules.customer.entity.CsLinkman;
/**
* <p>
* 联系人Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsLinkmanMapper extends BaseMapper<CsLinkman> {

}
