package com.yf.modules.sys.config.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.modules.sys.config.entity.CfgUploadOss;
/**
* <p>
* 阿里云上传配置Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-02-05 11:16
*/
public interface CfgUploadOssMapper extends BaseMapper<CfgUploadOss> {

}
