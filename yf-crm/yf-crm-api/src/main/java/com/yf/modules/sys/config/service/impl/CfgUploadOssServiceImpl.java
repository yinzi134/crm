package com.yf.modules.sys.config.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.utils.BeanMapper;
import com.yf.modules.sys.config.dto.CfgUploadOssDTO;
import com.yf.modules.sys.config.entity.CfgUploadOss;
import com.yf.modules.sys.config.mapper.CfgUploadOssMapper;
import com.yf.modules.sys.config.service.CfgUploadOssService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
* <p>
* 阿里云上传配置业务实现类
* </p>
*
* @author 聪明笨狗
* @since 2021-02-05 11:16
*/
@Service
public class CfgUploadOssServiceImpl extends ServiceImpl<CfgUploadOssMapper, CfgUploadOss> implements CfgUploadOssService {


    @Override
    public void save(CfgUploadOssDTO reqDTO) {
        CfgUploadOss entity = new CfgUploadOss();
        BeanMapper.copy(reqDTO, entity);
        this.saveOrUpdate(entity);
    }

    @Override
    public CfgUploadOssDTO find() {
       List<CfgUploadOss> list =  this.list();
       CfgUploadOssDTO respDTO = new CfgUploadOssDTO();
       if(!CollectionUtils.isEmpty(list)){
           BeanMapper.copy(list.get(0), respDTO);
       }
       return respDTO;
    }
}
