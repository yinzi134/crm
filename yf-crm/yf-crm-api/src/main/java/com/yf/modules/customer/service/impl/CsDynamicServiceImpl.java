package com.yf.modules.customer.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsDynamicDTO;
import com.yf.modules.customer.entity.CsDynamic;
import com.yf.modules.customer.mapper.CsDynamicMapper;
import com.yf.modules.customer.service.CsDynamicService;
import org.springframework.stereotype.Service;

/**
* <p>
* 客户动态业务实现类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-09 16:16
*/
@Service
public class CsDynamicServiceImpl extends ServiceImpl<CsDynamicMapper, CsDynamic> implements CsDynamicService {

    @Override
    public IPage<CsDynamicDTO> paging(PagingReqDTO<CsDynamicDTO> reqDTO) {

        // 创建分页对象
        Page query = new Page(reqDTO.getCurrent(), reqDTO.getSize());


        //查询条件
        QueryWrapper<CsDynamic> wrapper = new QueryWrapper<>();

        // 请求参数
        CsDynamicDTO params = reqDTO.getParams();

        wrapper.lambda().orderByDesc(CsDynamic::getCreateTime);

        //获得数据
        IPage<CsDynamic> page = this.page(query, wrapper);
        //转换结果
        IPage<CsDynamicDTO> pageData = JSON.parseObject(JSON.toJSONString(page), new TypeReference<Page<CsDynamicDTO>>(){});
        return pageData;
     }

    @Override
    public void save(String customerId, String content, Integer dataType, Integer mood) {
        new Thread(() -> {
            CsDynamic dynamic = new CsDynamic();
            dynamic.setContent(content);
            dynamic.setDataType(dataType);
            dynamic.setCustomerId(customerId);
            dynamic.setMood(mood);
            save(dynamic);
        }).start();
    }

}
