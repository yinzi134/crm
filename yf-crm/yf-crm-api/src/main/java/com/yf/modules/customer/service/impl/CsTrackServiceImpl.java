package com.yf.modules.customer.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.boot.base.api.utils.StringUtils;
import com.yf.modules.customer.dto.CsCustomerDTO;
import com.yf.modules.customer.dto.CsTrackDTO;
import com.yf.modules.customer.entity.CsTrack;
import com.yf.modules.customer.enums.DynamicType;
import com.yf.modules.customer.mapper.CsTrackMapper;
import com.yf.modules.customer.service.CsDynamicService;
import com.yf.modules.customer.service.CsTrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
* <p>
* 客户跟进历史业务实现类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Service
public class CsTrackServiceImpl extends ServiceImpl<CsTrackMapper, CsTrack> implements CsTrackService {

    @Autowired
    private CsDynamicService dynamicService;

    @Override
    public IPage<CsTrackDTO> paging(PagingReqDTO<CsTrackDTO> reqDTO) {

        // 创建分页对象
        Page query = new Page(reqDTO.getCurrent(), reqDTO.getSize());


        //查询条件
        QueryWrapper<CsTrack> wrapper = new QueryWrapper<>();

        // 请求参数
        CsTrackDTO params = reqDTO.getParams();

        // 倒序排列
        if(params!=null){

            if(!StringUtils.isBlank(params.getCustomerId())){
                wrapper.lambda()
                        .eq(CsTrack::getCustomerId, params.getCustomerId());
            }
        }

        wrapper.lambda().orderByDesc(CsTrack::getStartTime);

        //获得数据
        IPage<CsTrack> page = this.page(query, wrapper);
        //转换结果
        IPage<CsTrackDTO> pageData = JSON.parseObject(JSON.toJSONString(page), new TypeReference<Page<CsTrackDTO>>(){});
        return pageData;
     }

    @Override
    public void saveBatch(String userId, List<String> ids) {

        List<CsTrack> list = new ArrayList<>();

        for(String id: ids){
            CsTrack item = new CsTrack();
            item.setCustomerId(id);
            item.setCreateBy(userId);
            item.setStartTime(new Date());
            item.setIsCurrent(true);

            list.add(item);
        }

        this.saveBatch(list);

    }

    @Override
    public void release(String userId, String customerId, String result, String remark) {
        //查询条件
        QueryWrapper<CsTrack> wrapper = new QueryWrapper<>();
        wrapper.lambda()
                .eq(CsTrack::getCreateBy, userId)
                .eq(CsTrack::getCustomerId, customerId)
                .eq(CsTrack::getIsCurrent, true);

        CsTrack track = new CsTrack();
        track.setIsCurrent(false);
        track.setResult(result);
        track.setEndTime(new Date());
        track.setRemark(remark);
        this.update(track, wrapper);

        // 记录动态
        dynamicService.save(customerId, "释放了客户："+remark, DynamicType.RELEASE, result.equals(2)?1:3);

    }

    @Override
    public IPage<CsTrackDTO> pagingHistory(PagingReqDTO<CsCustomerDTO> reqDTO) {
        return baseMapper.pagingHistory(reqDTO.toPage(), reqDTO.getParams());
    }


}
