package com.yf.modules.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.modules.customer.entity.CsDynamic;
/**
* <p>
* 客户动态Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-07-09 16:16
*/
public interface CsDynamicMapper extends BaseMapper<CsDynamic> {

}
