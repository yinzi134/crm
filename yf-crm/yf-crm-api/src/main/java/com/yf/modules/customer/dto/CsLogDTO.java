package com.yf.modules.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
* <p>
* 客户修改日志数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="客户修改日志", description="客户修改日志")
public class CsLogDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "ID", required=true)
    private String id;

    @ApiModelProperty(value = "客户ID")
    private String customerId;

    @ApiModelProperty(value = "修改时间")
    private Date updateTime;

    @ApiModelProperty(value = "修改人")
    private String updateBy;

    @ApiModelProperty(value = "旧JSON数据")
    private String oldData;

    @ApiModelProperty(value = "新JSON数据")
    private String newData;

}
