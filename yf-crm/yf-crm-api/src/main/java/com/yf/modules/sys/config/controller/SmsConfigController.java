package com.yf.modules.sys.config.controller;

import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.boot.base.api.api.dto.BaseIdRespDTO;
import com.yf.boot.base.api.utils.BeanMapper;
import com.yf.modules.sys.config.dto.CfgSmsAliyunDTO;
import com.yf.modules.sys.config.entity.CfgSmsAliyun;
import com.yf.modules.sys.config.service.CfgSmsAliyunService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
* <p>
* 阿里大鱼短信控制器
* </p>
*
* @author 聪明笨狗
* @since 2021-03-30 18:17
*/
@Api(tags={"阿里大鱼短信"})
@RestController
@RequestMapping("/api/sys/config/sms")
public class SmsConfigController extends BaseController {

    @Autowired
    private CfgSmsAliyunService baseService;

    /**
    * 添加或修改
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "添加或修改")
    @RequestMapping(value = "/save", method = { RequestMethod.POST})
    public ApiRest<BaseIdRespDTO> save(@RequestBody CfgSmsAliyunDTO reqDTO) {
        //复制参数
        CfgSmsAliyun entity = new CfgSmsAliyun();
        BeanMapper.copy(reqDTO, entity);
        baseService.saveOrUpdate(entity);
        return super.success(new BaseIdRespDTO(entity.getId()));
    }

    /**
     * 配置详情
     * @return
     */
    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/detail", method = { RequestMethod.POST})
    public ApiRest<CfgSmsAliyunDTO> find() {
        CfgSmsAliyunDTO dto = baseService.find();
        return super.success(dto);
    }
}
