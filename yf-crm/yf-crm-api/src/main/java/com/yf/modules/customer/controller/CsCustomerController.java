package com.yf.modules.customer.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.boot.base.api.api.dto.BaseIdReqDTO;
import com.yf.boot.base.api.api.dto.BaseIdsReqDTO;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsCustomerDTO;
import com.yf.modules.customer.dto.request.CustomerSaveReqDTO;
import com.yf.modules.customer.dto.request.ReleaseReqDTO;
import com.yf.modules.customer.entity.CsCustomer;
import com.yf.modules.customer.service.CsCustomerService;
import com.yf.modules.sys.user.utils.UserUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
* <p>
* 客户信息控制器
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Api(tags={"客户信息"})
@RestController
@RequestMapping("/api/customer/customer")
public class CsCustomerController extends BaseController {

    @Autowired
    private CsCustomerService baseService;

    /**
    * 添加或修改
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "添加或修改")
    @RequestMapping(value = "/save", method = { RequestMethod.POST})
    public ApiRest save(@RequestBody CustomerSaveReqDTO reqDTO) {
        baseService.save(reqDTO);
        return super.success();
    }

    /**
    * 批量删除
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "批量删除")
    @RequestMapping(value = "/delete", method = { RequestMethod.POST})
    public ApiRest edit(@RequestBody BaseIdsReqDTO reqDTO) {
        //根据ID删除
        baseService.removeByIds(reqDTO.getIds());
        return super.success();
    }

    /**
    * 查找详情
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/detail", method = { RequestMethod.POST})
    public ApiRest<CsCustomerDTO> find(@RequestBody BaseIdReqDTO reqDTO) {
        CsCustomer entity = baseService.getById(reqDTO.getId());
        CsCustomerDTO dto = new CsCustomerDTO();
        BeanUtils.copyProperties(entity, dto);
        return super.success(dto);
    }

    /**
    * 客户公海
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "客户公海")
    @RequestMapping(value = "/paging-sea", method = { RequestMethod.POST})
    public ApiRest<IPage<CsCustomerDTO>> pagingSea(@RequestBody PagingReqDTO<CsCustomerDTO> reqDTO) {

        //分页查询并转换
        IPage<CsCustomerDTO> page = baseService.pagingSea(reqDTO);

        return super.success(page);
    }

    /**
     * 保留客户
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "保留客户")
    @RequestMapping(value = "/paging-keep", method = { RequestMethod.POST})
    public ApiRest<IPage<CsCustomerDTO>> pagingKeep(@RequestBody PagingReqDTO<CsCustomerDTO> reqDTO) {

        //分页查询并转换
        IPage<CsCustomerDTO> page = baseService.pagingKeep(reqDTO);

        return super.success(page);
    }


    /**
     * 认领客户
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "认领客户")
    @RequestMapping(value = "/got", method = { RequestMethod.POST})
    public ApiRest got(@RequestBody BaseIdsReqDTO reqDTO) {
        baseService.got(UserUtils.getUserId(), reqDTO.getIds());
        return super.success();
    }

    /**
     * 释放客户
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "释放客户")
    @RequestMapping(value = "/release", method = { RequestMethod.POST})
    public ApiRest release(@RequestBody ReleaseReqDTO reqDTO) {
        baseService.release(UserUtils.getUserId(), reqDTO.getResult(), reqDTO.getRemark(), reqDTO.getIds());
        return super.success();
    }



}
