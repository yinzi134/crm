package com.yf.modules.customer.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yf.modules.customer.dto.CsCustomerDTO;
import com.yf.modules.customer.entity.CsCustomer;
import org.apache.ibatis.annotations.Param;

/**
* <p>
* 客户信息Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsCustomerMapper extends BaseMapper<CsCustomer> {

    /**
     * 客户公海
     * @param page
     * @param reqDTO
     * @return
     */
    IPage<CsCustomerDTO> pagingSea(Page page, @Param("query") CsCustomerDTO reqDTO);

    /**
     * 保留客户
     * @param page
     * @param reqDTO
     * @return
     */
    IPage<CsCustomerDTO> pagingKeep(Page page, @Param("query") CsCustomerDTO reqDTO);
}
