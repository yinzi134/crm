package com.yf.modules.customer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import java.util.Date;

/**
* <p>
* 客户动态实体类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-09 16:16
*/
@Data
@TableName("cs_dynamic")
public class CsDynamic extends Model<CsDynamic> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    /**
     * 客户ID
     */
    @TableField("customer_id")
    private String customerId;

    /**
     * 操作人
     */
    @TableField("create_by")
    private String createBy;

    /**
     * 内容
     */
    private String content;

    /**
     * 所属部门
     */
    @TableField("dept_code")
    private String deptCode;

    /**
     * 操作时间
     */
    @TableField("create_time")
    private Date createTime;

    /**
     * 1联系,2认领,3释放,4签约
     */
    @TableField("data_type")
    private Integer dataType;

    /**
     * 1开心,2一般,3难过
     */
    private Integer mood;

}
