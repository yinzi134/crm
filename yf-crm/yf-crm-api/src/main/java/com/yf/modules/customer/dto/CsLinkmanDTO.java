package com.yf.modules.customer.dto;

import com.yf.boot.base.api.annon.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
* <p>
* 联系人数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="联系人", description="联系人")
public class CsLinkmanDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "ID", required=true)
    private String id;

    @ApiModelProperty(value = "客户ID")
    private String customerId;

    @ApiModelProperty(value = "姓名")
    private String name;

    @Dict(dicCode = "position")
    @ApiModelProperty(value = "职位")
    private String position;

    @ApiModelProperty(value = "手机号码")
    private String mobile;

    @ApiModelProperty(value = "邮箱")
    private String email;

    @ApiModelProperty(value = "微信号码")
    private String wechat;

    @ApiModelProperty(value = "备注信息")
    private String remark;

    @ApiModelProperty(value = "主要联系人")
    private Boolean isMain;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private String createBy;

}
