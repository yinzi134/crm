package com.yf.modules.sys.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.modules.sys.config.dto.CfgSmsAliyunDTO;
import com.yf.modules.sys.config.entity.CfgSmsAliyun;

/**
* <p>
* 阿里大鱼短信业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-03-30 18:17
*/
public interface CfgSmsAliyunService extends IService<CfgSmsAliyun> {

    /**
     * 查找短信配置
     * @return
     */
    CfgSmsAliyunDTO find();
}
