package com.yf.modules.customer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import java.util.Date;

/**
* <p>
* 联系记录实体类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@TableName("cs_contact")
public class CsContact extends Model<CsContact> {

    private static final long serialVersionUID = 1L;

    /**
    * ID
    */
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    /**
    * 联系方法
    */
    @TableField("contact_type")
    private String contactType;

    /**
    * 客户ID
    */
    @TableField("customer_id")
    private String customerId;

    /**
    * 联系人ID
    */
    @TableField("contact_id")
    private String contactId;

    /**
    * 联系时间
    */
    @TableField("create_time")
    private Date createTime;

    /**
    * 联系人
    */
    @TableField("create_by")
    private String createBy;

    /**
    * 联系结果
    */
    @TableField("result")
    private String result;

    /**
    * 备注信息
    */
    private String remark;

}
