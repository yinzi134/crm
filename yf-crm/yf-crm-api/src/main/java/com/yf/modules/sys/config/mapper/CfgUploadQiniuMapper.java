package com.yf.modules.sys.config.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yf.modules.sys.config.entity.CfgUploadQiniu;
/**
* <p>
* 七牛云上传配置Mapper
* </p>
*
* @author 聪明笨狗
* @since 2021-02-24 19:21
*/
public interface CfgUploadQiniuMapper extends BaseMapper<CfgUploadQiniu> {

}
