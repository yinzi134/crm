package com.yf.modules.customer.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsLogDTO;
import com.yf.modules.customer.entity.CsLog;
import com.yf.modules.customer.mapper.CsLogMapper;
import com.yf.modules.customer.service.CsLogService;
import org.springframework.stereotype.Service;

/**
* <p>
* 客户修改日志业务实现类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Service
public class CsLogServiceImpl extends ServiceImpl<CsLogMapper, CsLog> implements CsLogService {

    @Override
    public IPage<CsLogDTO> paging(PagingReqDTO<CsLogDTO> reqDTO) {

        // 创建分页对象
        Page query = new Page(reqDTO.getCurrent(), reqDTO.getSize());


        //查询条件
        QueryWrapper<CsLog> wrapper = new QueryWrapper<>();

        // 请求参数
        CsLogDTO params = reqDTO.getParams();


        //获得数据
        IPage<CsLog> page = this.page(query, wrapper);
        //转换结果
        IPage<CsLogDTO> pageData = JSON.parseObject(JSON.toJSONString(page), new TypeReference<Page<CsLogDTO>>(){});
        return pageData;
     }
}
