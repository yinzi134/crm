package com.yf.modules.customer.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.Data;
import java.util.Date;

/**
* <p>
* 客户跟进历史实体类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@TableName("cs_track")
public class CsTrack extends Model<CsTrack> {

    private static final long serialVersionUID = 1L;

    /**
    * ID
    */
    @TableId(value = "id", type = IdType.ID_WORKER_STR)
    private String id;

    /**
    * 客户ID
    */
    @TableField("customer_id")
    private String customerId;

    /**
    * 用户ID
    */
    @TableField("create_by")
    private String createBy;

    /**
    * 部门ID
    */
    @TableField("dept_code")
    private String deptCode;

    /**
    * 开始跟进日期
    */
    @TableField("start_time")
    private Date startTime;

    /**
    * 结束跟进日期
    */
    @TableField("end_time")
    private Date endTime;

    /**
    * 跟进结果
    */
    private String result;

    /**
    * 正在跟进
    */
    @TableField("is_current")
    private Boolean isCurrent;

    /**
     * 备注信息
     */
    private String remark;

}
