package com.yf.modules.sys.config.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.utils.BeanMapper;
import com.yf.modules.sys.config.dto.CfgBaseDTO;
import com.yf.modules.sys.config.entity.CfgBase;
import com.yf.modules.sys.config.mapper.CfgBaseMapper;
import com.yf.modules.sys.config.service.CfgBaseService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
* <p>
* 语言设置 服务实现类
* </p>
*
* @author 聪明笨狗
* @since 2020-04-17 09:12
*/
@Service
public class CfgBaseServiceImpl extends ServiceImpl<CfgBaseMapper, CfgBase> implements CfgBaseService {


    @Override
    public CfgBaseDTO findSimple() {

        QueryWrapper<CfgBase> wrapper = new QueryWrapper<>();
        wrapper.last(" LIMIT 1");

        CfgBase entity = this.getOne(wrapper, false);
        CfgBaseDTO dto = new CfgBaseDTO();
        BeanMapper.copy(entity, dto);
        return dto;
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(CfgBaseDTO reqDTO) {
        //复制参数
        CfgBase entity = new CfgBase();
        BeanMapper.copy(reqDTO, entity);
        this.saveOrUpdate(entity);
    }

}
