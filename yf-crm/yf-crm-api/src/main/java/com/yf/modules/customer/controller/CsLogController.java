package com.yf.modules.customer.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.boot.base.api.api.dto.BaseIdReqDTO;
import com.yf.boot.base.api.api.dto.BaseIdRespDTO;
import com.yf.boot.base.api.api.dto.BaseIdsReqDTO;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.boot.base.api.utils.BeanMapper;
import com.yf.modules.customer.dto.CsLogDTO;
import com.yf.modules.customer.entity.CsLog;
import com.yf.modules.customer.service.CsLogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
* <p>
* 客户修改日志控制器
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Api(tags={"客户修改日志"})
@RestController
@RequestMapping("/api/customer/log")
public class CsLogController extends BaseController {

    @Autowired
    private CsLogService baseService;

    /**
    * 添加或修改
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "添加或修改")
    @RequestMapping(value = "/save", method = { RequestMethod.POST})
    public ApiRest<BaseIdRespDTO> save(@RequestBody CsLogDTO reqDTO) {
        //复制参数
        CsLog entity = new CsLog();
        BeanMapper.copy(reqDTO, entity);
        baseService.saveOrUpdate(entity);
        return super.success(new BaseIdRespDTO(entity.getId()));
    }

    /**
    * 批量删除
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "批量删除")
    @RequestMapping(value = "/delete", method = { RequestMethod.POST})
    public ApiRest edit(@RequestBody BaseIdsReqDTO reqDTO) {
        //根据ID删除
        baseService.removeByIds(reqDTO.getIds());
        return super.success();
    }

    /**
    * 查找详情
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/detail", method = { RequestMethod.POST})
    public ApiRest<CsLogDTO> find(@RequestBody BaseIdReqDTO reqDTO) {
        CsLog entity = baseService.getById(reqDTO.getId());
        CsLogDTO dto = new CsLogDTO();
        BeanUtils.copyProperties(entity, dto);
        return super.success(dto);
    }

    /**
    * 分页查找
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "分页查找")
    @RequestMapping(value = "/paging", method = { RequestMethod.POST})
    public ApiRest<IPage<CsLogDTO>> paging(@RequestBody PagingReqDTO<CsLogDTO> reqDTO) {

        //分页查询并转换
        IPage<CsLogDTO> page = baseService.paging(reqDTO);

        return super.success(page);
    }

    /**
     * 查找列表，每次最多返回200条数据
     * @param reqDTO
     * @return
     */

    @ApiOperation(value = "查找列表")
    @RequestMapping(value = "/list", method = { RequestMethod.POST})
    public ApiRest<List<CsLogDTO>> list(@RequestBody CsLogDTO reqDTO) {

        //分页查询并转换
        QueryWrapper<CsLog> wrapper = new QueryWrapper<>();

        //转换并返回
        List<CsLog> list = baseService.list(wrapper);

        //转换数据
        List<CsLogDTO> dtoList = BeanMapper.mapList(list, CsLogDTO.class);

        return super.success(dtoList);
    }
}
