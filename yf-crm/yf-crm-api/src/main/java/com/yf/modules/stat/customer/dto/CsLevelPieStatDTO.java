package com.yf.modules.stat.customer.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
* <p>
* 客户跟进历史数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="客户跟进历史", description="客户跟进历史")
public class CsLevelPieStatDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "名称", required=true)
    private String name;

    @ApiModelProperty(value = "值")
    private Integer value;

}
