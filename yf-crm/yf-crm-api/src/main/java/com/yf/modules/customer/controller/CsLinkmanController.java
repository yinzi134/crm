package com.yf.modules.customer.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.boot.base.api.api.dto.BaseIdReqDTO;
import com.yf.boot.base.api.api.dto.BaseIdRespDTO;
import com.yf.boot.base.api.api.dto.BaseIdsReqDTO;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.boot.base.api.utils.BeanMapper;
import com.yf.modules.customer.dto.CsLinkmanDTO;
import com.yf.modules.customer.entity.CsLinkman;
import com.yf.modules.customer.service.CsLinkmanService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
* <p>
* 联系人控制器
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Api(tags={"联系人"})
@RestController
@RequestMapping("/api/customer/linkman")
public class CsLinkmanController extends BaseController {

    @Autowired
    private CsLinkmanService baseService;

    /**
    * 添加或修改
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "添加或修改")
    @RequestMapping(value = "/save", method = { RequestMethod.POST})
    public ApiRest<BaseIdRespDTO> save(@RequestBody CsLinkmanDTO reqDTO) {
        //复制参数
        CsLinkman entity = new CsLinkman();
        BeanMapper.copy(reqDTO, entity);
        baseService.saveOrUpdate(entity);
        return super.success(new BaseIdRespDTO(entity.getId()));
    }

    /**
    * 批量删除
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "批量删除")
    @RequestMapping(value = "/delete", method = { RequestMethod.POST})
    public ApiRest edit(@RequestBody BaseIdsReqDTO reqDTO) {
        //根据ID删除
        baseService.removeByIds(reqDTO.getIds());
        return super.success();
    }

    /**
    * 查找详情
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/detail", method = { RequestMethod.POST})
    public ApiRest<CsLinkmanDTO> find(@RequestBody BaseIdReqDTO reqDTO) {
        CsLinkman entity = baseService.getById(reqDTO.getId());
        CsLinkmanDTO dto = new CsLinkmanDTO();
        BeanUtils.copyProperties(entity, dto);
        return super.success(dto);
    }

    /**
    * 分页查找
    * @param reqDTO
    * @return
    */
    @ApiOperation(value = "分页查找")
    @RequestMapping(value = "/paging", method = { RequestMethod.POST})
    public ApiRest<IPage<CsLinkmanDTO>> paging(@RequestBody PagingReqDTO<CsLinkmanDTO> reqDTO) {
        //分页查询并转换
        IPage<CsLinkmanDTO> page = baseService.paging(reqDTO);
        return super.success(page);
    }

    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/mark", method = { RequestMethod.POST})
    public ApiRest mark(@RequestBody BaseIdReqDTO reqDTO) {
        baseService.markMain(reqDTO.getId());
        return super.success();
    }

}
