package com.yf.modules.sys.config.service.impl;

import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.utils.BeanMapper;
import com.yf.modules.sys.config.dto.CfgSmsAliyunDTO;
import com.yf.modules.sys.config.entity.CfgSmsAliyun;
import com.yf.modules.sys.config.mapper.CfgSmsAliyunMapper;
import com.yf.modules.sys.config.service.CfgSmsAliyunService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

/**
* <p>
* 阿里大鱼短信业务实现类
* </p>
*
* @author 聪明笨狗
* @since 2021-03-30 18:17
*/
@Service
public class CfgSmsAliyunServiceImpl extends ServiceImpl<CfgSmsAliyunMapper, CfgSmsAliyun> implements CfgSmsAliyunService {


    @Override
    public CfgSmsAliyunDTO find() {
        List<CfgSmsAliyun> list = this.list();

        if(!CollectionUtils.isEmpty(list)){
            CfgSmsAliyunDTO dto = BeanMapper.map(list.get(0), CfgSmsAliyunDTO.class);
            return dto;
        }else{

            CfgSmsAliyun conf = new CfgSmsAliyun();
            conf.setAccessKeyId("");
            conf.setAccessKeySecret("");
            conf.setId(IdWorker.getIdStr());
            conf.setSign("");
            this.save(conf);

            return this.find();
        }
    }
}
