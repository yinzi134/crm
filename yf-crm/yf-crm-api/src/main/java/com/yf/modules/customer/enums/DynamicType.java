package com.yf.modules.customer.enums;

/**
 * 客户动态类型
 * @author bool
 */
public interface DynamicType {

    /**
     * 联系
     */
    Integer CONTACT = 1;

    /**
     * 认领客户
     */
    Integer GOT = 2;

    /**
     * 释放客户
     */
    Integer RELEASE = 3;

    /**
     * 签约
     */
    Integer SIGN = 4;
}
