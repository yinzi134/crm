package com.yf.modules.sys.config.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.modules.sys.config.dto.CfgUploadOssDTO;
import com.yf.modules.sys.config.entity.CfgUploadOss;

/**
* <p>
* 阿里云上传配置业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-02-05 11:16
*/
public interface CfgUploadOssService extends IService<CfgUploadOss> {

    /**
     * 保存配置
     * @param reqDTO
     */
    void save(CfgUploadOssDTO reqDTO);

    /**
     * 查找配置
     * @return
     */
    CfgUploadOssDTO find();
}
