package com.yf.modules.customer.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsLogDTO;
import com.yf.modules.customer.entity.CsLog;

/**
* <p>
* 客户修改日志业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsLogService extends IService<CsLog> {

    /**
    * 分页查询数据
    * @param reqDTO
    * @return
    */
    IPage<CsLogDTO> paging(PagingReqDTO<CsLogDTO> reqDTO);
}
