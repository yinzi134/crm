package com.yf.modules.stat.customer.controller;

import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.modules.customer.dto.CsContactDTO;
import com.yf.modules.stat.customer.dto.CsLevelPieStatDTO;
import com.yf.modules.stat.customer.dto.CsTotalStatDTO;
import com.yf.modules.stat.customer.mapper.CsStatMapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
* <p>
* 联系记录控制器
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Api(tags={"联系记录"})
@RestController
@RequestMapping("/api/customer/stat")
public class CsStatController extends BaseController {

    @Autowired
    private CsStatMapper csStatMapper;

    /**
     * 客户等级饼图
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "客户等级饼图")
    @RequestMapping(value = "/level-pie", method = { RequestMethod.POST})
    public ApiRest<List<CsLevelPieStatDTO>> levelPie(@RequestBody CsContactDTO reqDTO) {

        //转换并返回
        List<CsLevelPieStatDTO> list = csStatMapper.findLevelPie();
        return super.success(list);
    }

    /**
     * 总体统计
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "总体统计")
    @RequestMapping(value = "/total-stat", method = { RequestMethod.POST})
    public ApiRest<CsTotalStatDTO> totalStat(@RequestBody CsContactDTO reqDTO) {

        //转换并返回
        CsTotalStatDTO stat = csStatMapper.findTotal();
        return super.success(stat);
    }
}
