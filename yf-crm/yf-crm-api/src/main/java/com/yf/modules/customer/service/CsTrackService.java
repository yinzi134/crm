package com.yf.modules.customer.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsCustomerDTO;
import com.yf.modules.customer.dto.CsTrackDTO;
import com.yf.modules.customer.entity.CsTrack;

import java.util.List;

/**
* <p>
* 客户跟进历史业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
public interface CsTrackService extends IService<CsTrack> {

    /**
    * 分页查询数据
    * @param reqDTO
    * @return
    */
    IPage<CsTrackDTO> paging(PagingReqDTO<CsTrackDTO> reqDTO);

    /**
     * 批量保存
     * @param userId
     * @param ids
     */
    void saveBatch(String userId, List<String> ids);

    /**
     * 保存释放记录
     * @param userId
     * @param customerId
     * @param result
     * @param remark
     */
    void release(String userId, String customerId, String result, String remark);


    /**
     * 查找追踪历史
     * @param reqDTO
     * @return
     */
    IPage<CsTrackDTO> pagingHistory(PagingReqDTO<CsCustomerDTO> reqDTO);
}
