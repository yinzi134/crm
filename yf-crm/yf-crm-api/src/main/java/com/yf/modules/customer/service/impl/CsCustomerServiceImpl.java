package com.yf.modules.customer.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.boot.base.api.utils.BeanMapper;
import com.yf.boot.base.api.utils.StringUtils;
import com.yf.modules.customer.dto.CsCustomerDTO;
import com.yf.modules.customer.dto.CsLinkmanDTO;
import com.yf.modules.customer.dto.request.CustomerSaveReqDTO;
import com.yf.modules.customer.entity.CsCustomer;
import com.yf.modules.customer.entity.CsLinkman;
import com.yf.modules.customer.enums.DynamicType;
import com.yf.modules.customer.mapper.CsCustomerMapper;
import com.yf.modules.customer.service.CsCustomerService;
import com.yf.modules.customer.service.CsDynamicService;
import com.yf.modules.customer.service.CsLinkmanService;
import com.yf.modules.customer.service.CsTrackService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
* <p>
* 客户信息业务实现类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Service
public class CsCustomerServiceImpl extends ServiceImpl<CsCustomerMapper, CsCustomer> implements CsCustomerService {

    @Autowired
    private CsTrackService customerTrackService;

    @Autowired
    private CsLinkmanService contactsService;

    @Autowired
    private CsDynamicService dynamicService;

    @Override
    public IPage<CsCustomerDTO> pagingSea(PagingReqDTO<CsCustomerDTO> reqDTO) {
        return baseMapper.pagingSea(reqDTO.toPage(), reqDTO.getParams());
    }

    @Override
    public IPage<CsCustomerDTO> pagingKeep(PagingReqDTO<CsCustomerDTO> reqDTO) {
        return baseMapper.pagingKeep(reqDTO.toPage(), reqDTO.getParams());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void got(String userId, List<String> ids) {

         //查询条件
         QueryWrapper<CsCustomer> wrapper = new QueryWrapper<>();
         wrapper.lambda().in(CsCustomer::getId, ids);

         // 更新为自己的客户
         CsCustomer customer = new CsCustomer();
         customer.setIsPublic(false);
         customer.setOwnerId(userId);
         this.update(customer, wrapper);

         // 增加联系记录
         customerTrackService.saveBatch(userId, ids);

        for(String id: ids){
            // 记录动态
            dynamicService.save(id, "认领了客户，并发誓一定要成单！", DynamicType.GOT, 1);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void release(String userId, String result, String remark, List<String> ids) {
        for(String id: ids){
            customerTrackService.release(userId, id, result, remark);
        }


        //查询条件
        QueryWrapper<CsCustomer> wrapper = new QueryWrapper<>();
        wrapper.lambda().in(CsCustomer::getId, ids);

        // 更新为公海客户
        CsCustomer customer = new CsCustomer();
        customer.setIsPublic(true);
        customer.setOwnerId("");
        this.update(customer, wrapper);

    }

    @Override
    public void updateContact(String id, String userId) {
        CsCustomer customer = new CsCustomer();
        customer.setId(id);
        customer.setContactTime(new Date());
        this.updateById(customer);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void save(CustomerSaveReqDTO reqDTO) {


        //复制参数
        CsCustomer entity = new CsCustomer();
        BeanMapper.copy(reqDTO, entity);

        // 添加的
        if(StringUtils.isBlank(reqDTO.getId())){

            entity.setId(IdWorker.getIdStr());

            // 保存联系人
            CsLinkmanDTO contactsDTO = reqDTO.getContacts();
            if(contactsDTO!=null){
                CsLinkman c = new CsLinkman();
                BeanMapper.copy(contactsDTO, c);
                c.setId(null);
                c.setCustomerId(entity.getId());
                c.setIsMain(true);
                contactsService.save(c);

                entity.setMobile(contactsDTO.getMobile());
                entity.setEmail(contactsDTO.getEmail());
                entity.setContact(contactsDTO.getName());
            }

            this.save(entity);
        }else{

            this.updateById(entity);
        }


    }

}
