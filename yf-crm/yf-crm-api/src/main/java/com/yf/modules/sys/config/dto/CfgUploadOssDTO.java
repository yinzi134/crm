package com.yf.modules.sys.config.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
* <p>
* 阿里云上传配置数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-02-05 11:16
*/
@Data
@ApiModel(value="阿里云上传配置", description="阿里云上传配置")
public class CfgUploadOssDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "ID", required=true)
    private String id;

    @ApiModelProperty(value = "存储Bucket")
    private String bucket;

    @ApiModelProperty(value = "秘钥ID")
    private String accessKeyId;

    @ApiModelProperty(value = "秘钥密码")
    private String accessKeySecret;

    @ApiModelProperty(value = "节点")
    private String endpoint;

    @ApiModelProperty(value = "访问路径")
    private String url;

    @ApiModelProperty(value = "智能媒体项目名")
    private String project;

    @ApiModelProperty(value = "MPS管道ID")
    private String pipeline;

}
