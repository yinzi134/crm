package com.yf.modules.customer.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.boot.base.api.api.dto.BaseIdReqDTO;
import com.yf.boot.base.api.api.dto.BaseIdsReqDTO;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsCustomerDTO;
import com.yf.modules.customer.dto.CsTrackDTO;
import com.yf.modules.customer.entity.CsTrack;
import com.yf.modules.customer.service.CsTrackService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
* <p>
* 客户跟进历史控制器
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Api(tags={"客户跟进历史"})
@RestController
@RequestMapping("/api/customer/track")
public class CsTrackController extends BaseController {

    @Autowired
    private CsTrackService baseService;

    /**
    * 批量删除
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "批量删除")
    @RequestMapping(value = "/delete", method = { RequestMethod.POST})
    public ApiRest edit(@RequestBody BaseIdsReqDTO reqDTO) {
        //根据ID删除
        baseService.removeByIds(reqDTO.getIds());
        return super.success();
    }

    /**
    * 查找详情
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "查找详情")
    @RequestMapping(value = "/detail", method = { RequestMethod.POST})
    public ApiRest<CsTrackDTO> find(@RequestBody BaseIdReqDTO reqDTO) {
        CsTrack entity = baseService.getById(reqDTO.getId());
        CsTrackDTO dto = new CsTrackDTO();
        BeanUtils.copyProperties(entity, dto);
        return super.success(dto);
    }

    /**
    * 分页查找
    * @param reqDTO
    * @return
    */

    @ApiOperation(value = "分页查找")
    @RequestMapping(value = "/paging", method = { RequestMethod.POST})
    public ApiRest<IPage<CsTrackDTO>> paging(@RequestBody PagingReqDTO<CsTrackDTO> reqDTO) {

        //分页查询并转换
        IPage<CsTrackDTO> page = baseService.paging(reqDTO);

        return super.success(page);
    }


    /**
     * 分页查找
     * @param reqDTO
     * @return
     */
    @ApiOperation(value = "客户追踪历史")
    @RequestMapping(value = "/paging-history", method = { RequestMethod.POST})
    public ApiRest<IPage<CsTrackDTO>> pagingHistory(@RequestBody PagingReqDTO<CsCustomerDTO> reqDTO) {

        //分页查询并转换
        IPage<CsTrackDTO> page = baseService.pagingHistory(reqDTO);

        return super.success(page);
    }

}
