package com.yf.modules.customer.dto;

import com.yf.boot.base.api.annon.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
* <p>
* 客户动态数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-09 16:16
*/
@Data
@ApiModel(value="客户动态", description="客户动态")
public class CsDynamicDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "ID", required=true)
    private String id;

    @Dict(dictTable = "cs_customer", dicCode = "id", dicText = "name")
    @ApiModelProperty(value = "客户ID")
    private String customerId;

    @Dict(dictTable = "sys_user", dicCode = "id", dicText = "real_name")
    @ApiModelProperty(value = "操作人")
    private String createBy;

    @ApiModelProperty(value = "内容")
    private String content;

    @ApiModelProperty(value = "操作时间")
    private Date createTime;

    @ApiModelProperty(value = "所属部门")
    private String deptCode;


    @Dict(dicCode = "dynamic_type")
    @ApiModelProperty(value = "1联系,2认领,3释放,4签约")
    private Integer dataType;

    @ApiModelProperty(value = "1开心,2一般,3难过")
    private Integer mood;

}
