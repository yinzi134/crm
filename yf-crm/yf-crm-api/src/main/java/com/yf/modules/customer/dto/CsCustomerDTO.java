package com.yf.modules.customer.dto;

import com.yf.boot.base.api.annon.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;

import java.io.Serializable;

/**
* <p>
* 客户信息数据传输类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-07 12:01
*/
@Data
@ApiModel(value="客户信息", description="客户信息")
public class CsCustomerDTO implements Serializable {

    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "客户ID", required=true)
    private String id;

    @Dict(dicCode = "cus_type")
    @ApiModelProperty(value = "客户类型,企业/个人")
    private String cusType;

    @ApiModelProperty(value = "客户名称")
    private String name;

    @ApiModelProperty(value = "备注信息")
    private String remark;

    @ApiModelProperty(value = "联系人")
    private String contact;

    @ApiModelProperty(value = "手机号")
    private String mobile;

    @ApiModelProperty(value = "微信号")
    private String wechat;

    @ApiModelProperty(value = "电子邮箱")
    private String email;

    @ApiModelProperty(value = "联系地址")
    private String address;

    @ApiModelProperty(value = "是否公海")
    private Boolean isPublic;

    @ApiModelProperty(value = "客户行业")
    private String industry;

    @ApiModelProperty(value = "主营产品")
    private String product;

    @Dict(dicCode = "level")
    @ApiModelProperty(value = "客户级别")
    private String level;

    @Dict(dicCode = "source")
    @ApiModelProperty(value = "客户来源")
    private String source;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "创建人")
    private String createBy;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "更新时间")
    private String updateBy;

    @ApiModelProperty(value = "最近联系时间")
    private Date contactTime;

    @ApiModelProperty(value = "当前归属部门")
    private String deptCode;

    @Dict(dictTable = "sys_user", dicCode = "id", dicText = "real_name")
    @ApiModelProperty(value = "归属人ID")
    private String ownerId;

    @ApiModelProperty(value = "客户状态")
    private String state;

}
