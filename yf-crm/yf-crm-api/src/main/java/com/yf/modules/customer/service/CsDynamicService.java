package com.yf.modules.customer.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.yf.boot.base.api.api.dto.PagingReqDTO;
import com.yf.modules.customer.dto.CsDynamicDTO;
import com.yf.modules.customer.entity.CsDynamic;

/**
* <p>
* 客户动态业务类
* </p>
*
* @author 聪明笨狗
* @since 2021-07-09 16:16
*/
public interface CsDynamicService extends IService<CsDynamic> {

    /**
    * 分页查询数据
    * @param reqDTO
    * @return
    */
    IPage<CsDynamicDTO> paging(PagingReqDTO<CsDynamicDTO> reqDTO);

    /**
     * 保存动态
     * @param customerId
     * @param content
     * @param dataType
     * @param mood
     */
    void save(String customerId, String content, Integer dataType, Integer mood);
}
