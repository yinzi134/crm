package com.yf.boot.ability.qiniu.controller;


import com.qiniu.util.Auth;
import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.boot.ability.qiniu.dto.QiniuTokenDTO;
import com.yf.modules.sys.config.dto.CfgUploadQiniuDTO;
import com.yf.modules.sys.config.service.CfgUploadQiniuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 本地文件上传下载请求类
 * @author bool
 */
@Log4j2
@Api(tags = {"文件上传"})
@RestController
public class QiniuController extends BaseController {

    @Autowired
    private CfgUploadQiniuService cfgUploadQiniuService;

    /**
     * 获得上传Token
     * @return
     */
    @PostMapping("/api/ability/qiniu/token")
    @ApiOperation(value = "获得七牛Token")
    public ApiRest<QiniuTokenDTO> token() {

        CfgUploadQiniuDTO conf = cfgUploadQiniuService.find();

        // 上传并返回URL
        String accessKey = conf.getAccessKeyId();
        String secretKey = conf.getAccessKeySecret();
        String bucket = conf.getBucket();
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);
        QiniuTokenDTO respDTO = new QiniuTokenDTO();
        respDTO.setToken(upToken);
        return super.success(respDTO);
    }
}
