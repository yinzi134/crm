package com.yf.boot.base.utils;

import org.springframework.util.CollectionUtils;

import java.util.List;

/**
 * 集合工具类
 * @author bool
 */
public class ListUtils {

    /**
     * 对比两个列表的值是否一样，不限顺序
     * @param list1
     * @param list2
     * @return
     */
    public static boolean compareList(List<String> list1, List<String> list2){
        if(list1.size()!=list2.size()){
            return false;
        }

        for(String l1: list1){
            if(!list2.contains(l1)){
                return false;
            }
        }
        return true;
    }

    /**
     * 列表转换String
     * @param list
     * @return
     */
    public static String listToString(List<String> list){

        if(CollectionUtils.isEmpty(list)){
            return "";
        }

        StringBuffer sb = null;
        for(String str: list){
            if(sb == null){
                sb = new StringBuffer();
            }else{
                sb.append(",");
            }

            sb.append(str);
        }

        return sb.toString();
    }


}
