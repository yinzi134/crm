package com.yf.boot.base.utils;

import org.springframework.core.io.ClassPathResource;

import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * 资源工具
 * @author bool
 */
public class ResourceUtil {


    /**
     * 数据流转换btye[]
     * @param input
     * @return
     * @throws IOException
     */
    public static byte[] toByteArray(InputStream input) throws IOException {
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024*4];
        int n = 0;
        while (-1 != (n = input.read(buffer))) {
            output.write(buffer, 0, n);
        }
        return output.toByteArray();
    }


    /**
     * 从资源文件写出流
     * @param response
     * @param path
     * @throws Exception
     */
    public static void write(HttpServletResponse response, String path) throws Exception{

        // 获取文件读成流
        InputStream fis = new ClassPathResource(path).getInputStream();
        byte [] data = toByteArray(fis);

        // 设置响应头
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setContentType("application/octet-stream; charset=utf-8");

        // 输出流文件
        OutputStream os = new BufferedOutputStream(response.getOutputStream());
        os.write(data);
        os.flush();
        os.close();
    }
}
