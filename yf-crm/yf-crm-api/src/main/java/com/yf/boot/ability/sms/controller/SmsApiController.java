package com.yf.boot.ability.sms.controller;


import com.yf.boot.base.api.api.ApiRest;
import com.yf.boot.base.api.api.controller.BaseController;
import com.yf.boot.ability.sms.dto.request.SendSmsReqDTO;
import com.yf.boot.ability.sms.service.SmsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * 短信验证码请求类
 * @author bool
 * @date 2020-03-07 10:56
 */
@Api(tags = {"公共能力"})
@RestController
@RequestMapping("/api/ability/sms")
public class SmsApiController extends BaseController {

    @Autowired
    private SmsService smsService;

    @RequestMapping(value="/send", method = RequestMethod.POST)
    @ApiOperation(value = "发送短信验证码")
    public ApiRest send(@RequestBody SendSmsReqDTO reqDTO) {
        smsService.sendSms(reqDTO.getMobile(), reqDTO.getCaptchaKey(), reqDTO.getCaptchaValue());
        return success();
    }
}
