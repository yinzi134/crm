package com.yf.boot.base.aspect.dict;

/**
 * 数据字典查询接口，必须要先实现才能进行字典的翻译
 * @author bool
 */
public interface BaseDictService {


    /**
     * 查找数据字典值
     * @param dicTable
     * @param dicText
     * @param dicCode
     * @param value
     * @return
     */
    String findDictText(String dicTable, String dicText, String dicCode, String value);
}
