package com.yf.boot.ability.qiniu.service;

/**
 * 视频转换业务类
 * @author bool
 */
public interface QiniuService {

    /**
     * 转换视频得到新的地址
     * @param input
     * @return
     */
    String convertVideo(String input);

    /**
     * 文件上传
     * @param localFile
     * @return
     */
    String upload(String localFile);
}
