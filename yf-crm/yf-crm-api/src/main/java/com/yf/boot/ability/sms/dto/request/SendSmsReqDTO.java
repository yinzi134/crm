package com.yf.boot.ability.sms.dto.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 阿里云短信验证码返回参数
 * @author bool
 * @date 2019-12-16 10:16
 */
@Data
@ApiModel(value="短信验证码发送请求类", description="短信验证码发送请求类")
public class SendSmsReqDTO implements Serializable {

    /**
     * 消息
     */
    @ApiModelProperty(value = "手机号", required = true)
    private String mobile;

    /**
     * 验证码键
     */
    @ApiModelProperty(value = "前端产生的验证码键（预留的，暂时不传）")
    private String captchaKey;

    /**
     * 用户输入的验证码
     */
    @ApiModelProperty(value = "用户输入的验证码（预留的，暂时不传）")
    private String captchaValue;

}
