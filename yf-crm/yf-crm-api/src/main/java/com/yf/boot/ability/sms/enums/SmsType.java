package com.yf.boot.ability.sms.enums;

/**
 * 短信验证码类型
 */
public interface SmsType {

    /**
     * 短信验证码
     */
    Integer VALIDATE_CODE = 1;
}
