package com.yf.boot.ability.sms.dto.response;

import lombok.Data;

import java.io.Serializable;

/**
 * 阿里云短信验证码返回参数
 * @author bool
 * @date 2019-12-16 10:16
 */
@Data
public class SendRespDTO implements Serializable {

    /**
     * 消息
     */
    private String Message;
    /**
     * 请求ID
     */
    private String RequestId;
    /**
     * 业务ID
     */
    private String BizId;
    /**
     * 发送编号
     */
    private String Code;
}
