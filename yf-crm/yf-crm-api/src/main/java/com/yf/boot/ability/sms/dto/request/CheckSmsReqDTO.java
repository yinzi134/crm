package com.yf.boot.ability.sms.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 校验短信验证码
 * @author bool
 * @date 2019-12-16 10:16
 */
@Data
public class CheckSmsReqDTO implements Serializable {


    /**
     * 手机号码
     */
    @ApiModelProperty(value = "用户的手机号")
    private String mobile;

    /**
     * 验证码
     */
    @ApiModelProperty(value = "用户收到的验证码")
    private String code;


}
