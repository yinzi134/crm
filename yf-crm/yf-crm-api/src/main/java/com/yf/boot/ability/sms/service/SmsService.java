package com.yf.boot.ability.sms.service;


/**
 * 短信服务类
 * @author bool
 * @date 2020-02-17 09:43
 */
public interface SmsService {

    /**
     * 发送验证码
     * @param mobile
     * @param captchaKey
     * @param captchaValue
     * @return
     */
    boolean sendSms(String mobile,
                    String captchaKey,
                    String captchaValue);

    /**
     * 校验验证码
     * @param mobile
     * @param sms
     * @return
     */
    boolean checkSms(String mobile, String sms);
}
