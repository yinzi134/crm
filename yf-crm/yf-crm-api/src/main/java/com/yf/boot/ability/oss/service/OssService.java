package com.yf.boot.ability.oss.service;

/**
 * OSS云文档转换
 * @author bool
 */
public interface OssService {

    /**
     * 上传文件
     * @param localFile
     * @return
     */
    String upload(String localFile);
}
