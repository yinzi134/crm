package com.yf.boot.ability.qiniu.dto;


import com.yf.boot.base.api.api.dto.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 文件上传请求类
 * @author
 * @date 2019-12-26 17:54
 */
@Data
@ApiModel(value="获得七牛上传Token", description="获得七牛上传Token")
public class QiniuTokenDTO extends BaseDTO {

    @ApiModelProperty(value = "获得上传Token", required=true)
    private String token;

}
