/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1-本地库
 Source Server Type    : MySQL
 Source Server Version : 80028
 Source Host           : localhost:3306
 Source Schema         : yf_crm

 Target Server Type    : MySQL
 Target Server Version : 80028
 File Encoding         : 65001

 Date: 02/08/2022 15:32:05
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cs_contact
-- ----------------------------
DROP TABLE IF EXISTS `cs_contact`;
CREATE TABLE `cs_contact` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `contact_type` varchar(255) DEFAULT NULL COMMENT '联系方法',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '客户ID',
  `contact_id` varchar(32) DEFAULT NULL COMMENT '联系人ID',
  `create_time` datetime DEFAULT NULL COMMENT '联系时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '联系人',
  `result` varchar(32) DEFAULT NULL COMMENT '联系结果',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='联系记录';

-- ----------------------------
-- Records of cs_contact
-- ----------------------------
BEGIN;
INSERT INTO `cs_contact` (`id`, `contact_type`, `customer_id`, `contact_id`, `create_time`, `create_by`, `result`, `remark`) VALUES ('1412680907797602306', '1', '1412627093463797761', '1412672624525438978', '2021-07-07 15:52:29', '10001', '2', '对方一听声音就挂了');
INSERT INTO `cs_contact` (`id`, `contact_type`, `customer_id`, `contact_id`, `create_time`, `create_by`, `result`, `remark`) VALUES ('1412707867743539201', '1', '1412627093463797761', '1412672624525438978', '2021-07-07 17:39:37', '10001', '1', '112');
INSERT INTO `cs_contact` (`id`, `contact_type`, `customer_id`, `contact_id`, `create_time`, `create_by`, `result`, `remark`) VALUES ('1412708264461783041', '3', '1412627093463797761', '1412672624525438978', '2021-07-07 17:41:12', '10001', '3', '对方友好地回复了我');
INSERT INTO `cs_contact` (`id`, `contact_type`, `customer_id`, `contact_id`, `create_time`, `create_by`, `result`, `remark`) VALUES ('1412985022096470017', '2', '1412978611052466177', '1412981820038967298', '2021-07-08 12:00:56', '10001', '1', '邮件还没回复我');
INSERT INTO `cs_contact` (`id`, `contact_type`, `customer_id`, `contact_id`, `create_time`, `create_by`, `result`, `remark`) VALUES ('1413423750849884161', '1', '1412978149666443265', '1412978149817438209', '2021-07-09 17:04:17', '10001', '1', '联系了雷军，他不鸟我。');
INSERT INTO `cs_contact` (`id`, `contact_type`, `customer_id`, `contact_id`, `create_time`, `create_by`, `result`, `remark`) VALUES ('1413453170828840962', '3', '1412764696980877314', '1413453059876917250', '2021-07-09 19:01:11', '10001', '3', '聊了半个小时，意向还是很强的');
INSERT INTO `cs_contact` (`id`, `contact_type`, `customer_id`, `contact_id`, `create_time`, `create_by`, `result`, `remark`) VALUES ('1413463321623302145', '3', '1412627093463797761', '1412779616405323778', '2021-07-09 19:41:31', '1412700694430924802', '2', '很好啊~~');
INSERT INTO `cs_contact` (`id`, `contact_type`, `customer_id`, `contact_id`, `create_time`, `create_by`, `result`, `remark`) VALUES ('1503282925332684802', '2', '1412764696980877314', '1413453059876917250', '2022-03-14 16:12:34', '10001', '3', 'xxx');
COMMIT;

-- ----------------------------
-- Table structure for cs_customer
-- ----------------------------
DROP TABLE IF EXISTS `cs_customer`;
CREATE TABLE `cs_customer` (
  `id` varchar(32) NOT NULL COMMENT '客户ID',
  `cus_type` varchar(32) NOT NULL DEFAULT '1' COMMENT '客户类型,企业/个人',
  `name` varchar(255) NOT NULL COMMENT '客户名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `contact` varchar(255) DEFAULT NULL COMMENT '联系人',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `wechat` varchar(255) DEFAULT NULL COMMENT '微信号',
  `email` varchar(255) DEFAULT NULL COMMENT '电子邮箱',
  `address` varchar(255) DEFAULT NULL COMMENT '联系地址',
  `is_public` tinyint NOT NULL DEFAULT '1' COMMENT '是否公海',
  `industry` varchar(255) DEFAULT NULL COMMENT '客户行业',
  `product` varchar(255) DEFAULT NULL COMMENT '主营产品',
  `level` varchar(255) DEFAULT NULL COMMENT '客户级别',
  `source` varchar(255) DEFAULT NULL COMMENT '客户来源',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `update_by` varchar(255) DEFAULT NULL COMMENT '更新时间',
  `contact_time` datetime DEFAULT NULL COMMENT '最近联系时间',
  `dept_code` varchar(255) DEFAULT NULL COMMENT '当前归属部门',
  `owner_id` varchar(255) DEFAULT NULL COMMENT '归属人ID',
  `state` varchar(255) DEFAULT NULL COMMENT '客户状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='客户信息';

-- ----------------------------
-- Records of cs_customer
-- ----------------------------
BEGIN;
INSERT INTO `cs_customer` (`id`, `cus_type`, `name`, `remark`, `contact`, `mobile`, `wechat`, `email`, `address`, `is_public`, `industry`, `product`, `level`, `source`, `create_time`, `create_by`, `update_time`, `update_by`, `contact_time`, `dept_code`, `owner_id`, `state`) VALUES ('1412627093463797761', '1', '深圳银湖网络科技有限公司', NULL, '郭经理', '18682216559', NULL, '18365918@qq.com', NULL, 0, '1', '软件服务', '3', '2', '2021-07-07 12:18:39', '10001', '2021-07-09 19:41:31', '1412700694430924802', '2021-07-09 19:41:31', 'A01A01', '1412700694430924802', NULL);
INSERT INTO `cs_customer` (`id`, `cus_type`, `name`, `remark`, `contact`, `mobile`, `wechat`, `email`, `address`, `is_public`, `industry`, `product`, `level`, `source`, `create_time`, `create_by`, `update_time`, `update_by`, `contact_time`, `dept_code`, `owner_id`, `state`) VALUES ('1412978149666443265', '1', '小米公司', NULL, '雷军', '18166778899', NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, '2021-07-08 11:33:37', '10001', '2021-07-09 18:54:45', '10001', '2021-07-09 17:04:17', 'A01', '', NULL);
INSERT INTO `cs_customer` (`id`, `cus_type`, `name`, `remark`, `contact`, `mobile`, `wechat`, `email`, `address`, `is_public`, `industry`, `product`, `level`, `source`, `create_time`, `create_by`, `update_time`, `update_by`, `contact_time`, `dept_code`, `owner_id`, `state`) VALUES ('1412978611052466177', '1', '大米科技有限公司', '很有实力的一个公司', '王总', '18788776655', NULL, '1827211@qq.com', '深圳市南山区白石洲东二坊', 1, '2', '游戏辅助设备', '3', '2', '2021-07-08 11:35:27', '10001', '2021-07-09 18:54:57', '10001', '2021-07-08 12:00:56', 'A01', '', NULL);
COMMIT;

-- ----------------------------
-- Table structure for cs_dynamic
-- ----------------------------
DROP TABLE IF EXISTS `cs_dynamic`;
CREATE TABLE `cs_dynamic` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '客户ID',
  `create_by` varchar(255) DEFAULT NULL COMMENT '操作人',
  `content` varchar(255) DEFAULT NULL COMMENT '内容',
  `dept_code` varchar(255) DEFAULT NULL COMMENT '所属部门',
  `create_time` datetime DEFAULT NULL COMMENT '操作时间',
  `data_type` int DEFAULT NULL COMMENT '1联系,2领取,3释放,4签约',
  `mood` int DEFAULT NULL COMMENT '1开心,2一般,3难过',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='客户动态';

-- ----------------------------
-- Records of cs_dynamic
-- ----------------------------
BEGIN;
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1', '1412627093463797761', '10001', '联系了客户，并', NULL, '2021-07-09 16:43:07', NULL, NULL);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413423412721872897', '1412978149666443265', '10001', '领取了客户，并发誓一定要成单！', 'A01', '2021-07-09 17:02:56', 2, 1);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413423750904410113', '1412978149666443265', '10001', '联系了客户：联系了雷军，他不鸟我。', 'A01', '2021-07-09 17:04:17', 1, 3);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413434295246147585', '1412978611052466177', '10001', '释放了客户：不想努力了', 'A01', '2021-07-09 17:46:11', 3, 3);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413444208479444994', '1412978611052466177', '10001', '领取了客户，并发誓一定要成单！', 'A01', '2021-07-09 18:25:34', 2, 1);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413444221137854466', '1412764696980877314', '10001', '领取了客户，并发誓一定要成单！', 'A01', '2021-07-09 18:25:38', 2, 1);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413451550753755137', '1412978149666443265', '10001', '释放了客户：已经谈成了交易', 'A01', '2021-07-09 18:54:45', 3, 3);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413451600057798657', '1412978611052466177', '10001', '释放了客户：太难跟进了', 'A01', '2021-07-09 18:54:57', 3, 3);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413453170879172609', '1412764696980877314', '10001', '联系了客户：聊了半个小时，意向还是很强的', 'A01', '2021-07-09 19:01:11', 1, 3);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1413463321702993921', '1412627093463797761', '1412700694430924802', '联系了客户：很好啊~~', 'A01A01', '2021-07-09 19:41:31', 1, 3);
INSERT INTO `cs_dynamic` (`id`, `customer_id`, `create_by`, `content`, `dept_code`, `create_time`, `data_type`, `mood`) VALUES ('1503282925362044929', '1412764696980877314', '10001', '联系了客户：xxx', 'A01', '2022-03-14 16:12:34', 1, 3);
COMMIT;

-- ----------------------------
-- Table structure for cs_linkman
-- ----------------------------
DROP TABLE IF EXISTS `cs_linkman`;
CREATE TABLE `cs_linkman` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '客户ID',
  `name` varchar(255) DEFAULT NULL COMMENT '姓名',
  `position` varchar(255) DEFAULT NULL COMMENT '职位',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `wechat` varchar(255) DEFAULT NULL COMMENT '微信号码',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `is_main` varchar(255) DEFAULT NULL COMMENT '主要联系人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `create_by` varchar(255) DEFAULT NULL COMMENT '创建人',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='联系人';

-- ----------------------------
-- Records of cs_linkman
-- ----------------------------
BEGIN;
INSERT INTO `cs_linkman` (`id`, `customer_id`, `name`, `position`, `mobile`, `email`, `wechat`, `remark`, `is_main`, `create_time`, `create_by`) VALUES ('1412672624525438978', NULL, '郭银花', '1', '18603038204', '835487894@qq.com', '1811112', NULL, 'true', '2021-07-07 15:19:35', '10001');
INSERT INTO `cs_linkman` (`id`, `customer_id`, `name`, `position`, `mobile`, `email`, `wechat`, `remark`, `is_main`, `create_time`, `create_by`) VALUES ('1412778646736764930', NULL, '郭银花', '1', '18166053622', '835487894@qq.com', '111', NULL, NULL, '2021-07-07 22:20:52', '1412700694430924802');
INSERT INTO `cs_linkman` (`id`, `customer_id`, `name`, `position`, `mobile`, `email`, `wechat`, `remark`, `is_main`, `create_time`, `create_by`) VALUES ('1412779180990431233', NULL, '111', NULL, '22', NULL, NULL, NULL, NULL, '2021-07-07 22:23:00', '1412700694430924802');
INSERT INTO `cs_linkman` (`id`, `customer_id`, `name`, `position`, `mobile`, `email`, `wechat`, `remark`, `is_main`, `create_time`, `create_by`) VALUES ('1412779616405323778', '1412627093463797761', '郭总', '1', '18166778877', '835487@qq.com', 'a1776621', NULL, '1', '2021-07-07 22:24:43', '1412700694430924802');
INSERT INTO `cs_linkman` (`id`, `customer_id`, `name`, `position`, `mobile`, `email`, `wechat`, `remark`, `is_main`, `create_time`, `create_by`) VALUES ('1412978149817438209', '1412978149666443265', '雷军', '1', '18166778899', NULL, NULL, NULL, '1', '2021-07-08 11:33:37', '10001');
INSERT INTO `cs_linkman` (`id`, `customer_id`, `name`, `position`, `mobile`, `email`, `wechat`, `remark`, `is_main`, `create_time`, `create_by`) VALUES ('1412978611081826305', '1412978611052466177', '王总', '1', '18788776655', '1827211@qq.com', 'a776688', '王总很随和', '1', '2021-07-08 11:35:27', '10001');
INSERT INTO `cs_linkman` (`id`, `customer_id`, `name`, `position`, `mobile`, `email`, `wechat`, `remark`, `is_main`, `create_time`, `create_by`) VALUES ('1412981820038967298', '1412978611052466177', '小木木', NULL, '1776112221', NULL, NULL, NULL, '0', '2021-07-08 11:48:12', '10001');
INSERT INTO `cs_linkman` (`id`, `customer_id`, `name`, `position`, `mobile`, `email`, `wechat`, `remark`, `is_main`, `create_time`, `create_by`) VALUES ('1413453059876917250', '1412764696980877314', '老刘', NULL, '18166556677', NULL, NULL, NULL, '1', '2021-07-09 19:00:45', '10001');
COMMIT;

-- ----------------------------
-- Table structure for cs_log
-- ----------------------------
DROP TABLE IF EXISTS `cs_log`;
CREATE TABLE `cs_log` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '客户ID',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `update_by` varchar(32) DEFAULT NULL COMMENT '修改人',
  `old_data` varchar(255) DEFAULT NULL COMMENT '旧JSON数据',
  `new_data` varchar(255) DEFAULT NULL COMMENT '新JSON数据',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='客户修改日志';

-- ----------------------------
-- Records of cs_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for cs_track
-- ----------------------------
DROP TABLE IF EXISTS `cs_track`;
CREATE TABLE `cs_track` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `customer_id` varchar(32) DEFAULT NULL COMMENT '客户ID',
  `create_by` varchar(32) DEFAULT NULL COMMENT '用户ID',
  `dept_code` varchar(255) DEFAULT NULL COMMENT '部门ID',
  `start_time` datetime DEFAULT NULL COMMENT '开始跟进日期',
  `end_time` datetime DEFAULT NULL COMMENT '结束跟进日期',
  `result` varchar(255) DEFAULT NULL COMMENT '跟进结果',
  `is_current` tinyint DEFAULT NULL COMMENT '正在跟进',
  `remark` varchar(2000) DEFAULT NULL COMMENT '备注信息',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='客户跟进历史';

-- ----------------------------
-- Records of cs_track
-- ----------------------------
BEGIN;
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1', '1412627093463797761', '10001', NULL, '2021-07-04 16:11:27', '2021-07-15 16:11:31', '1', 0, NULL);
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1412695400896303106', '1412627093463797761', '10001', 'A01', '2021-07-07 16:50:05', NULL, '1', 0, NULL);
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1412714272185696258', '1412627093463797761', '1412700694430924802', 'A01A01', '2021-07-07 18:05:04', '2021-07-07 20:08:50', '1', 0, NULL);
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1412714351814557698', '1412712813461291010', '1412713579886460930', 'A01A01A02', '2021-07-07 18:05:23', NULL, NULL, 1, '太难了');
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1412745542668251137', '1412627093463797761', '1412700694430924802', 'A01A01', '2021-07-07 20:09:20', '2021-07-07 21:39:34', '2', 0, NULL);
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1412776706258796545', '1412627093463797761', '1412700694430924802', 'A01A01', '2021-07-07 22:13:10', NULL, NULL, 1, NULL);
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1412984280166035457', '1412978611052466177', '10001', 'A01', '2021-07-08 11:57:59', '2021-07-09 17:46:11', '1', 0, '不想努力了');
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1413423412684124162', '1412978149666443265', '10001', 'A01', '2021-07-09 17:02:56', '2021-07-09 18:54:45', '2', 0, '已经谈成了交易');
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1413444208450084865', '1412978611052466177', '10001', 'A01', '2021-07-09 18:25:34', '2021-07-09 18:54:57', '2', 0, '太难跟进了');
INSERT INTO `cs_track` (`id`, `customer_id`, `create_by`, `dept_code`, `start_time`, `end_time`, `result`, `is_current`, `remark`) VALUES ('1413444221121077250', '1412764696980877314', '10001', 'A01', '2021-07-09 18:25:37', NULL, NULL, 1, NULL);
COMMIT;

-- ----------------------------
-- Table structure for el_cfg_base
-- ----------------------------
DROP TABLE IF EXISTS `el_cfg_base`;
CREATE TABLE `el_cfg_base` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `site_name` varchar(255) DEFAULT NULL COMMENT '系统名称',
  `front_logo` varchar(255) DEFAULT NULL COMMENT '前端LOGO',
  `back_logo` varchar(255) DEFAULT NULL COMMENT '后台LOGO',
  `copy_right` varchar(255) DEFAULT NULL COMMENT '版权信息',
  `tmp_dir` varchar(255) DEFAULT NULL COMMENT '系统临时目录',
  `upload_type` int NOT NULL DEFAULT '1' COMMENT '存储方案1本地2阿里云OSS',
  `live_type` int NOT NULL DEFAULT '0' COMMENT '1阿里云',
  `h5_code` varchar(500) DEFAULT NULL COMMENT '手机端二维码',
  `mp_code` varchar(500) DEFAULT NULL COMMENT '小程序二维码',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(255) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(255) NOT NULL DEFAULT '' COMMENT '修改人',
  `data_flag` int NOT NULL DEFAULT '0' COMMENT '数据标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='系统设置';

-- ----------------------------
-- Records of el_cfg_base
-- ----------------------------
BEGIN;
INSERT INTO `el_cfg_base` (`id`, `site_name`, `front_logo`, `back_logo`, `copy_right`, `tmp_dir`, `upload_type`, `live_type`, `h5_code`, `mp_code`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1', '云帆CRM', 'https://cdn.jeegen.com/2022/3/14/1647244744656-e34f7b4e.png', 'https://cdn.jeegen.com/2022/3/14/1647244739002-d0fd476c.png', '&copy; 2022 北京云帆互联科技有限公司', '/Users/bool/work/upload/temp/', 2, 1, '', '', '2020-12-03 16:51:30', '2022-03-14 15:59:29', '', '10001', 1);
COMMIT;

-- ----------------------------
-- Table structure for el_cfg_sms_aliyun
-- ----------------------------
DROP TABLE IF EXISTS `el_cfg_sms_aliyun`;
CREATE TABLE `el_cfg_sms_aliyun` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `access_key_id` varchar(255) DEFAULT NULL COMMENT '秘钥ID',
  `access_key_secret` varchar(255) DEFAULT NULL COMMENT '秘钥密码',
  `sign` varchar(255) DEFAULT NULL COMMENT '短信签名',
  `tmpl` varchar(255) DEFAULT NULL COMMENT '模板ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='阿里大鱼短信';

-- ----------------------------
-- Records of el_cfg_sms_aliyun
-- ----------------------------
BEGIN;
INSERT INTO `el_cfg_sms_aliyun` (`id`, `access_key_id`, `access_key_secret`, `sign`, `tmpl`) VALUES ('1376847714136113154', 'LTAIVRr5QrV9HNkD', 'CQH7BxLUDA2fcfe6B1pmNVGnZERHSC', '云帆互联', 'SMS_213693115');
COMMIT;

-- ----------------------------
-- Table structure for el_cfg_upload_local
-- ----------------------------
DROP TABLE IF EXISTS `el_cfg_upload_local`;
CREATE TABLE `el_cfg_upload_local` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `local_dir` varchar(255) DEFAULT NULL COMMENT '本地目录地址',
  `url` varchar(255) DEFAULT NULL COMMENT '访问路径',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='本地文件上传配置';

-- ----------------------------
-- Records of el_cfg_upload_local
-- ----------------------------
BEGIN;
INSERT INTO `el_cfg_upload_local` (`id`, `local_dir`, `url`) VALUES ('1357536107729895426', '/Users/bool/work/upload/', '/upload/file/');
COMMIT;

-- ----------------------------
-- Table structure for el_cfg_upload_oss
-- ----------------------------
DROP TABLE IF EXISTS `el_cfg_upload_oss`;
CREATE TABLE `el_cfg_upload_oss` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `bucket` varchar(255) DEFAULT NULL COMMENT '存储Bucket',
  `access_key_id` varchar(255) DEFAULT NULL COMMENT '秘钥ID',
  `access_key_secret` varchar(255) DEFAULT NULL COMMENT '秘钥密码',
  `endpoint` varchar(255) DEFAULT NULL COMMENT '节点',
  `url` varchar(255) DEFAULT NULL COMMENT '访问路径',
  `project` varchar(255) DEFAULT NULL COMMENT '智能媒体项目名',
  `pipeline` varchar(255) DEFAULT NULL COMMENT 'MPS管道ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='阿里云上传配置';

-- ----------------------------
-- Records of el_cfg_upload_oss
-- ----------------------------
BEGIN;
INSERT INTO `el_cfg_upload_oss` (`id`, `bucket`, `access_key_id`, `access_key_secret`, `endpoint`, `url`, `project`, `pipeline`) VALUES ('1357536107805392898', 'yfhl-files', 'LTAI5t5kUQde7fGu83s8cC23', 'Exli25RLrjpMWGxJUurj7Z566vVSNq', 'oss-cn-beijing', 'https://cdn.jeegen.com/', 'bjconv', 'fabae21dbe264fa5aad0bdb17295d9e3');
COMMIT;

-- ----------------------------
-- Table structure for el_cfg_upload_qiniu
-- ----------------------------
DROP TABLE IF EXISTS `el_cfg_upload_qiniu`;
CREATE TABLE `el_cfg_upload_qiniu` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `bucket` varchar(255) DEFAULT NULL COMMENT '存储Bucket',
  `access_key_id` varchar(255) DEFAULT NULL COMMENT '秘钥ID',
  `access_key_secret` varchar(255) DEFAULT NULL COMMENT '秘钥密码',
  `endpoint` varchar(255) DEFAULT NULL COMMENT '上传节点',
  `url` varchar(255) DEFAULT NULL COMMENT '访问路径',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='七牛云上传配置';

-- ----------------------------
-- Records of el_cfg_upload_qiniu
-- ----------------------------
BEGIN;
INSERT INTO `el_cfg_upload_qiniu` (`id`, `bucket`, `access_key_id`, `access_key_secret`, `endpoint`, `url`) VALUES ('1367734062541733889', 'yf-exam', 'edyB3_QoqGQWLXoKIONXUi63qvOGuMhpZAwfmitA', 'rn-M1FEN2OgZcFqfLJCb3LdfJDbLcY-_zlsg9toT', 'https://upload-z2.qiniup.com', 'http://qozlc75sb.hn-bkt.clouddn.com/');
COMMIT;

-- ----------------------------
-- Table structure for sys_depart
-- ----------------------------
DROP TABLE IF EXISTS `sys_depart`;
CREATE TABLE `sys_depart` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `dept_type` int NOT NULL DEFAULT '1' COMMENT '1公司2部门',
  `parent_id` varchar(32) NOT NULL COMMENT '所属上级',
  `dept_name` varchar(255) NOT NULL DEFAULT '' COMMENT '部门名称',
  `dept_code` varchar(255) NOT NULL DEFAULT '' COMMENT '部门编码',
  `sort` int NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(255) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(255) NOT NULL DEFAULT '' COMMENT '修改人',
  `data_flag` int NOT NULL DEFAULT '0' COMMENT '数据标识',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `dept_code` (`dept_code`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='部门信息';

-- ----------------------------
-- Records of sys_depart
-- ----------------------------
BEGIN;
INSERT INTO `sys_depart` (`id`, `dept_type`, `parent_id`, `dept_name`, `dept_code`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1342707003296395265', 1, '0', '云帆互联', 'A01', 1, '2020-12-26 13:41:10', '2020-12-26 13:41:10', '', '', 0);
INSERT INTO `sys_depart` (`id`, `dept_type`, `parent_id`, `dept_name`, `dept_code`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1342707524305764354', 1, '1342707003296395265', '市场部', 'A01A01', 1, '2020-12-26 13:43:14', '2020-12-26 13:43:14', '', '', 0);
INSERT INTO `sys_depart` (`id`, `dept_type`, `parent_id`, `dept_name`, `dept_code`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1406983962058194946', 1, '1342707524305764354', '销售A组', 'A01A01A01', 1, '2021-06-21 22:34:51', '2021-06-21 22:34:51', '', '', 0);
INSERT INTO `sys_depart` (`id`, `dept_type`, `parent_id`, `dept_name`, `dept_code`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1406984004097703938', 1, '1342707524305764354', '销售B组', 'A01A01A02', 2, '2021-06-21 22:35:01', '2021-06-21 22:35:01', '', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_dic
-- ----------------------------
DROP TABLE IF EXISTS `sys_dic`;
CREATE TABLE `sys_dic` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `code` varchar(255) DEFAULT NULL COMMENT '字典编码',
  `type` int DEFAULT NULL COMMENT '1分类字典,2数据字典',
  `title` varchar(255) DEFAULT NULL COMMENT '字典名称',
  `remark` varchar(255) DEFAULT NULL COMMENT '字典描述',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(255) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(255) NOT NULL DEFAULT '' COMMENT '修改人',
  `data_flag` int NOT NULL DEFAULT '0' COMMENT '数据标识',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE KEY `code` (`code`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='分类字典';

-- ----------------------------
-- Records of sys_dic
-- ----------------------------
BEGIN;
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399934295992238081', 'role_type', 1, '角色类型', '角色的主要类型', '2021-06-02 11:42:00', '2021-06-02 11:42:00', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399935914490269698', 'data_scope', 1, '数据权限', '角色数据权限范围', '2021-06-02 11:48:26', '2021-06-02 11:48:26', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412615242281676802', 'source', 1, '客户来源', '客户来源', '2021-07-07 11:31:33', '2021-07-07 11:31:33', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412615678476709889', 'level', 1, '客户级别', '客户级别', '2021-07-07 11:33:17', '2021-07-07 11:33:17', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412616392489857025', 'industry', 1, '客户行业', '客户行业', '2021-07-07 11:36:07', '2021-07-07 11:36:07', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617008721195010', 'position', 1, '职位', '职位', '2021-07-07 11:38:34', '2021-07-07 11:38:34', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412621142727938049', 'cus_type', 1, '客户类型', '客户类型', '2021-07-07 11:55:00', '2021-07-07 11:55:00', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412674698533154818', 'contact_type', 1, '联络方法', '联络方法', '2021-07-07 15:27:49', '2021-07-07 15:27:49', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412676746792153090', 'contact_result', 1, '联络结果', '联络结果', '2021-07-07 15:35:57', '2021-07-07 15:35:57', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412686345649823745', 'customer_track', 1, '客户追踪状态', '客户追踪状态', '2021-07-07 16:14:05', '2021-07-07 16:14:05', '', '', 0);
INSERT INTO `sys_dic` (`id`, `code`, `type`, `title`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413424082170515457', 'dynamic_type', 1, '客户动态类型', '客户动态类型', '2021-07-09 17:05:36', '2021-07-09 17:05:36', '', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_dic_value
-- ----------------------------
DROP TABLE IF EXISTS `sys_dic_value`;
CREATE TABLE `sys_dic_value` (
  `id` varchar(32) NOT NULL COMMENT 'ID/字典编码',
  `dic_code` varchar(255) DEFAULT NULL COMMENT '归属字典',
  `value` varchar(255) DEFAULT NULL COMMENT '子项编码',
  `title` varchar(255) DEFAULT NULL COMMENT '分类名称',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '上级ID',
  `remark` varchar(255) DEFAULT NULL COMMENT '描述',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(255) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(255) NOT NULL DEFAULT '' COMMENT '修改人',
  `data_flag` int NOT NULL DEFAULT '0' COMMENT '数据标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='分类字典值';

-- ----------------------------
-- Records of sys_dic_value
-- ----------------------------
BEGIN;
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399934414120615937', 'role_type', '1', '学员/前端用户', '0', '进入前端的用户', '2021-06-02 11:42:28', '2021-06-02 11:42:28', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399934480537419777', 'role_type', '2', '管理员/教师/后端用户', '0', '进入后端的用户', '2021-06-02 11:42:44', '2021-06-02 11:42:44', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399936107176595458', 'data_scope', '1', '仅本人数据', '0', '仅本人数据', '2021-06-02 11:49:12', '2021-06-02 11:49:12', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399936146162651137', 'data_scope', '2', '本部门数据', '0', '本部门数据', '2021-06-02 11:49:21', '2021-06-02 11:49:21', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399936188902608897', 'data_scope', '3', '本部门及以下', '0', '本部门及以下', '2021-06-02 11:49:31', '2021-06-02 11:49:31', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399936230719819777', 'data_scope', '4', '全部数据', '0', '全部数据', '2021-06-02 11:49:41', '2021-06-02 11:49:41', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412615293708038146', 'source', '1', '线上推广', '0', NULL, '2021-07-07 11:31:45', '2021-07-07 11:31:45', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412615357130108930', 'source', '2', '朋友介绍', '0', NULL, '2021-07-07 11:32:00', '2021-07-07 11:32:00', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412615479851249665', 'source', '3', '线下活动/展会对接', '0', NULL, '2021-07-07 11:32:30', '2021-07-07 11:32:30', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412615777651027969', 'level', '1', 'A:潜在客户', '0', NULL, '2021-07-07 11:33:41', '2021-07-07 11:33:41', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412615899805937666', 'level', '2', 'B:可跟进客户', '0', NULL, '2021-07-07 11:34:10', '2021-07-07 11:34:10', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412615956752003074', 'level', '3', 'C:重点跟进客户', '0', NULL, '2021-07-07 11:34:23', '2021-07-07 11:34:23', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412616031871987713', 'level', '4', 'D:拟成交客户', '0', NULL, '2021-07-07 11:34:41', '2021-07-07 11:34:41', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412616124545134593', 'level', '5', 'E:合作客户', '0', NULL, '2021-07-07 11:35:03', '2021-07-07 11:35:03', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412616229344014338', 'level', '6', 'F:与竞争对手合作的客户', '0', NULL, '2021-07-07 11:35:28', '2021-07-07 11:35:28', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412616674393223170', 'industry', '1', '软件服务商', '0', NULL, '2021-07-07 11:37:14', '2021-07-07 11:37:14', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412616770891575297', 'industry', '2', '互联网/游戏', '0', NULL, '2021-07-07 11:37:37', '2021-07-07 11:37:37', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617052069326849', 'position', '1', 'CEO', '0', NULL, '2021-07-07 11:38:45', '2021-07-07 11:38:45', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617084260610050', 'position', '2', '经理', '0', NULL, '2021-07-07 11:38:52', '2021-07-07 11:38:52', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617122143563778', 'position', '3', '客户经理', '0', NULL, '2021-07-07 11:39:01', '2021-07-07 11:39:01', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617160102014978', 'position', '4', '营销经理', '0', NULL, '2021-07-07 11:39:10', '2021-07-07 11:39:10', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617198664445954', 'position', '5', '业务主管', '0', NULL, '2021-07-07 11:39:19', '2021-07-07 11:39:19', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617226166497282', 'position', '6', '业务员', '0', NULL, '2021-07-07 11:39:26', '2021-07-07 11:39:26', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412621229348704257', 'cus_type', '1', '企业', '0', NULL, '2021-07-07 11:55:20', '2021-07-07 11:55:20', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412621249821102081', 'cus_type', '2', '个人', '0', NULL, '2021-07-07 11:55:25', '2021-07-07 11:55:25', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412674739394064385', 'contact_type', '1', '打电话', '0', NULL, '2021-07-07 15:27:58', '2021-07-07 15:27:58', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412674777302183938', 'contact_type', '2', '写邮件', '0', NULL, '2021-07-07 15:28:07', '2021-07-07 15:28:07', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412674976535818241', 'contact_type', '3', '微信/QQ/聊天工具', '0', NULL, '2021-07-07 15:28:55', '2021-07-07 15:28:55', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412675050418483202', 'contact_type', '4', '线下拜访', '0', NULL, '2021-07-07 15:29:12', '2021-07-07 15:29:12', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412676933740670978', 'contact_result', '1', '无响应', '0', NULL, '2021-07-07 15:36:41', '2021-07-07 15:36:41', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412677063017508866', 'contact_result', '2', '沟通中断', '0', '无意向中通挂断、长时间不响应', '2021-07-07 15:37:12', '2021-07-07 15:37:12', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412677104033607681', 'contact_result', '3', '沟通成功', '0', NULL, '2021-07-07 15:37:22', '2021-07-07 15:37:22', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412686458166222849', 'customer_track', '1', '放弃', '0', NULL, '2021-07-07 16:14:32', '2021-07-07 16:14:32', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412686484040884226', 'customer_track', '2', '成单', '0', NULL, '2021-07-07 16:14:38', '2021-07-07 16:14:38', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413424137187201026', 'dynamic_type', '1', '联系客户', '0', NULL, '2021-07-09 17:05:49', '2021-07-09 17:05:49', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413424186319278081', 'dynamic_type', '2', '认领客户', '0', NULL, '2021-07-09 17:06:00', '2021-07-09 17:06:00', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413424222079913985', 'dynamic_type', '3', '释放客户', '0', NULL, '2021-07-09 17:06:09', '2021-07-09 17:06:09', '', '', 0);
INSERT INTO `sys_dic_value` (`id`, `dic_code`, `value`, `title`, `parent_id`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413424255001006082', 'dynamic_type', '4', '签约客户', '0', NULL, '2021-07-09 17:06:17', '2021-07-09 17:06:17', '', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `parent_id` varchar(32) DEFAULT NULL COMMENT '上级菜单',
  `menu_type` int NOT NULL DEFAULT '1' COMMENT '1菜单2功能',
  `permission_tag` varchar(255) DEFAULT NULL COMMENT '权限标识',
  `path` varchar(255) DEFAULT NULL COMMENT '访问路径',
  `component` varchar(255) DEFAULT NULL COMMENT '视图或Layout',
  `redirect` varchar(255) DEFAULT NULL COMMENT '跳转地址',
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `meta_title` varchar(255) DEFAULT NULL COMMENT '路由标题',
  `meta_icon` varchar(255) DEFAULT NULL COMMENT '路由标题',
  `meta_active_menu` varchar(255) DEFAULT NULL COMMENT '高亮菜单',
  `meta_no_cache` tinyint DEFAULT NULL COMMENT '是否缓存',
  `hidden` tinyint DEFAULT NULL COMMENT '是否隐藏',
  `sort` int DEFAULT NULL COMMENT '越小越前',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(255) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(255) NOT NULL DEFAULT '' COMMENT '修改人',
  `data_flag` int NOT NULL DEFAULT '0' COMMENT '数据标识',
  PRIMARY KEY (`id`) USING BTREE,
  KEY `parent_id` (`parent_id`) USING BTREE,
  KEY `menu_type` (`menu_type`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529427607568', '0', 1, NULL, '/admin', 'Layout', '/admin/dashboard', 'Admin', '管理首页', 'component', NULL, NULL, 0, 3, '2021-03-07 11:08:18', '2021-06-02 18:18:14', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529427607569', '1367010529427607568', 1, NULL, '/admin/dashboard', 'dashboard', NULL, 'Dashboard', '管理首页', '', NULL, NULL, 0, 1, '2021-03-07 11:08:18', '2021-06-02 18:18:23', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996174', '0', 1, NULL, '/admin/sys', 'Layout', '/admin/sys/config', 'Sys', '系统设置', 'configure', NULL, NULL, 0, 10, '2021-03-07 11:08:18', '2021-07-07 11:40:38', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996175', '1367010529435996174', 1, 'sys:config', '/admin/sys/config', 'admin/sys/config', NULL, 'SysConfig', '系统配置', '', NULL, NULL, 0, 1, '2021-03-07 11:08:18', '2021-05-31 10:14:20', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996176', '1367010529435996174', 1, 'sys:menu', '/admin/sys/menu', 'admin/sys/menu', NULL, 'SysMenu', '菜单管理', '', NULL, NULL, 0, 2, '2021-03-07 11:08:18', '2021-05-31 10:14:26', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996177', '1367010529435996174', 1, 'sys:depart', '/admin/sys/depart', 'admin/sys/depart', '', 'SysDepart', '部门管理', '', NULL, NULL, 0, 4, '2021-03-07 11:08:18', '2021-05-31 10:14:36', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996178', '1367010529435996174', 1, 'sys:role', '/admin/sys/role', 'admin/sys/role', NULL, 'SysRole', '角色管理', '', NULL, NULL, 0, 3, '2021-03-07 11:08:18', '2021-05-31 10:14:31', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996179', '1367010529435996174', 1, 'sys:user', '/admin/sys/user', 'admin/sys/user', NULL, 'SysUser', '用户管理', '', NULL, NULL, 0, 5, '2021-03-07 11:08:18', '2021-05-31 10:14:41', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996180', '1367010529435996179', 2, 'sys:user:points', '/admin/sys/user/points/:id', 'admin/user/points', NULL, 'UserPoints', '积分明细', NULL, '/admin/sys/user', 1, 1, 6, '2021-03-07 11:08:18', '2021-03-07 11:08:18', '', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996183', '1367010529435996174', 1, 'sys:dict:catalog', '/admin/sys/dict-catalog', 'admin/sys/dict/catalog', NULL, 'DictCatalog', '分类字典', '', NULL, NULL, 0, 9, '2021-03-07 11:08:18', '2021-05-31 10:14:52', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1367010529435996184', '1367010529435996174', 1, 'sys:dict:value', '/admin/sys/dict-data', 'admin/sys/dict/dic', NULL, 'DictData', '数据字典', '', NULL, NULL, 0, 10, '2021-03-07 11:08:18', '2021-05-31 10:14:57', '', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399263684152385537', '1367010529435996175', 2, 'sys:config:update', NULL, NULL, NULL, NULL, '修改配置', NULL, NULL, NULL, NULL, 1, '2021-05-31 15:17:14', '2021-05-31 15:17:14', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399266644605394945', '1367010529435996176', 2, 'sys:menu:add', NULL, NULL, NULL, NULL, '添加菜单', NULL, NULL, NULL, NULL, 1, '2021-05-31 15:29:00', '2021-05-31 15:28:59', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399266713060630529', '1367010529435996176', 2, 'sys:menu:update', NULL, NULL, NULL, NULL, '修改菜单', NULL, NULL, NULL, NULL, 2, '2021-05-31 15:29:16', '2021-05-31 15:29:16', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399266787438223361', '1367010529435996176', 2, 'sys:menu:delete', NULL, NULL, NULL, NULL, '删除菜单', NULL, NULL, NULL, NULL, 3, '2021-05-31 15:29:34', '2021-05-31 15:29:33', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399266868300210177', '1367010529435996176', 2, 'sys:menu:sort', NULL, NULL, NULL, NULL, '菜单排序', NULL, NULL, NULL, NULL, 4, '2021-05-31 15:29:53', '2021-05-31 15:29:53', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399266973875036161', '1367010529435996178', 2, 'sys:role:add', NULL, NULL, NULL, NULL, '添加角色', NULL, NULL, NULL, NULL, 1, '2021-05-31 15:30:18', '2021-05-31 15:30:18', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399267041344610306', '1367010529435996178', 2, 'sys:role:update', NULL, NULL, NULL, NULL, '修改角色', NULL, NULL, NULL, NULL, 2, '2021-05-31 15:30:34', '2021-05-31 15:30:34', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399267166364229634', '1367010529435996178', 2, 'sys:role:delete', NULL, NULL, NULL, NULL, '删除角色', NULL, NULL, NULL, NULL, 3, '2021-05-31 15:31:04', '2021-05-31 15:31:04', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399270377145884673', '1367010529435996178', 2, 'sys:role:grant', NULL, NULL, NULL, NULL, '角色授权', NULL, NULL, NULL, NULL, 4, '2021-05-31 15:43:50', '2021-05-31 15:43:49', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399270608335921153', '1367010529435996177', 2, 'sys:depart:add', NULL, NULL, NULL, NULL, '添加部门', NULL, NULL, NULL, NULL, 1, '2021-05-31 15:44:45', '2021-05-31 15:44:44', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399270698039500801', '1367010529435996177', 2, 'sys:depart:update', NULL, NULL, NULL, NULL, '修改部门', NULL, NULL, NULL, NULL, 2, '2021-05-31 15:45:06', '2021-05-31 15:45:06', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399270777932603393', '1367010529435996177', 2, 'sys:depart:delete', NULL, NULL, NULL, NULL, '删除部门', NULL, NULL, NULL, NULL, 3, '2021-05-31 15:45:25', '2021-05-31 15:45:25', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399271425583472641', '1367010529435996179', 2, 'sys:user:add', NULL, NULL, NULL, NULL, '添加用户', NULL, NULL, NULL, NULL, 1, '2021-05-31 15:48:00', '2021-05-31 15:47:59', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399271482458234882', '1367010529435996179', 2, 'sys:user:update', NULL, NULL, NULL, NULL, '修改用户', NULL, NULL, NULL, NULL, 2, '2021-05-31 15:48:13', '2021-05-31 15:48:13', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399271543669907458', '1367010529435996179', 2, 'sys:user:delete', NULL, NULL, NULL, NULL, '删除用户', NULL, NULL, NULL, NULL, 3, '2021-05-31 15:48:28', '2021-05-31 15:48:27', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399271602981560321', '1367010529435996179', 2, 'sys:user:import', NULL, NULL, NULL, NULL, '导入用户', NULL, NULL, NULL, NULL, 4, '2021-05-31 15:48:42', '2021-05-31 15:48:42', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399271669553553410', '1367010529435996179', 2, 'sys:user:export', NULL, NULL, NULL, NULL, '导出用户', NULL, NULL, NULL, NULL, 5, '2021-05-31 15:48:58', '2021-05-31 15:48:57', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399272795329912833', '1367010529435996183', 2, 'sys:dict:catalog:add', NULL, NULL, NULL, NULL, '添加分类', NULL, NULL, NULL, NULL, 1, '2021-05-31 15:53:26', '2021-05-31 15:54:09', '10001', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399272872786124802', '1367010529435996183', 2, 'sys:dict:catalog:update', NULL, NULL, NULL, NULL, '修改分类', NULL, NULL, NULL, NULL, 2, '2021-05-31 15:53:45', '2021-05-31 15:53:44', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399272937902694402', '1367010529435996183', 2, 'sys:dict:catalog:delete', NULL, NULL, NULL, NULL, '删除分类', NULL, NULL, NULL, NULL, 3, '2021-05-31 15:54:00', '2021-05-31 15:54:00', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399273058870616065', '1367010529435996183', 2, 'sys:dict:catalog:sub', NULL, NULL, NULL, NULL, '维护子项', NULL, NULL, NULL, NULL, 4, '2021-05-31 15:54:29', '2021-05-31 15:54:29', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399273168916570113', '1367010529435996184', 2, 'sys:dict:value:add', NULL, NULL, NULL, NULL, '添加字典', NULL, NULL, NULL, NULL, 1, '2021-05-31 15:54:55', '2021-05-31 15:54:55', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399273214881947650', '1367010529435996184', 2, 'sys:dict:value:update', NULL, NULL, NULL, NULL, '修改字典', NULL, NULL, NULL, NULL, 2, '2021-05-31 15:55:06', '2021-05-31 15:55:18', '10001', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399273324386836482', '1367010529435996184', 2, 'sys:dict:value:delete', NULL, NULL, NULL, NULL, '删除字典', NULL, NULL, NULL, NULL, 3, '2021-05-31 15:55:32', '2021-05-31 15:55:32', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399273388576464897', '1367010529435996184', 2, 'sys:dict:value:sub', NULL, NULL, NULL, NULL, '维护子项', NULL, NULL, NULL, NULL, 4, '2021-05-31 15:55:48', '2021-05-31 15:55:47', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1399273800431951873', '1367010529435996179', 2, 'sys:user:state', NULL, NULL, NULL, NULL, '启用/禁用', NULL, NULL, NULL, NULL, 7, '2021-05-31 15:57:26', '2021-05-31 15:57:25', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617504391458817', '0', 1, NULL, '/admin/customer', 'Layout', NULL, 'Customer', '客户管理', 'peoples', NULL, NULL, NULL, 9, '2021-07-07 11:40:33', '2021-07-07 11:40:38', '10001', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617766615150593', '1412617504391458817', 1, 'customer:sea', '/admin/customer/sea', 'admin/customer/sea', NULL, 'CustomerSea', '客户公海', NULL, NULL, NULL, NULL, 1, '2021-07-07 11:41:35', '2021-07-07 11:41:35', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412617940552937474', '1412617504391458817', 1, 'customer:keep', '/admin/customer/keep', 'admin/customer/keep', NULL, 'CustomerKeep', '我的客户', NULL, NULL, NULL, NULL, 2, '2021-07-07 11:42:17', '2021-07-07 19:48:37', '10001', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412656531006078978', '1412617766615150593', 2, 'customer:add', '/admin/customer/add', 'admin/customer/form/add', NULL, 'CustomerAdd', '添加客户', NULL, '/admin/customer/sea', NULL, 1, 4, '2021-07-07 14:15:38', '2021-07-07 19:49:56', '10001', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412656811625988097', '1412617766615150593', 2, 'customer:update', '/admin/customer/update/:id', 'admin/customer/form/update', NULL, 'CustomerUpdate', '修改客户', NULL, '/admin/customer/sea', NULL, 1, 1, '2021-07-07 14:16:44', '2021-07-07 19:50:04', '10001', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412739504917311490', '1412617766615150593', 2, 'customer:delete', NULL, NULL, NULL, 'CustomerDelete', '删除客户', NULL, NULL, NULL, 1, 5, '2021-07-07 19:45:20', '2021-07-07 19:49:49', '10001', '10001', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412740598330421249', '1412617766615150593', 2, 'customer:import', NULL, NULL, NULL, 'CustomerImport', '导入客户', NULL, NULL, NULL, 1, 6, '2021-07-07 19:49:41', '2021-07-07 19:49:40', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412741211327950850', '1412617766615150593', 2, 'customer:got', NULL, NULL, NULL, 'CustomerGot', '领取客户', NULL, NULL, NULL, 1, 7, '2021-07-07 19:52:07', '2021-07-07 19:52:06', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412741344283193345', '1412617940552937474', 2, 'customer:release', NULL, NULL, NULL, 'CustomerRelase', '释放客户', NULL, NULL, NULL, 1, 1, '2021-07-07 19:52:39', '2021-07-07 19:52:38', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412741877110796290', '1412617940552937474', 2, 'customer:keep:add', '/admin/customer/add/:flag', 'admin/customer/form/add', NULL, 'CustomerKeepAdd', '添加客户', NULL, '/admin/customer/keep', NULL, 1, 2, '2021-07-07 19:54:46', '2021-07-07 19:54:45', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412742235132391426', '1412617940552937474', 2, 'customer:keep:update', '/admim/customer/update/:id/:flag', 'admin/customer/form/update', NULL, 'CustomerKeepUpdate', '修改客户', NULL, '/admin/customer/keep', NULL, 1, 3, '2021-07-07 19:56:11', '2021-07-07 19:56:10', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412742460639145985', '1412617940552937474', 2, 'customer:keep:delete', NULL, NULL, NULL, 'CustomerKeepDelete', '删除客户', NULL, NULL, NULL, 1, 1, '2021-07-07 19:57:05', '2021-07-07 19:57:04', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412742797139767298', '1412617940552937474', 2, 'customer:keep:export', NULL, NULL, NULL, 'CustomerKeepExport', '导出客户', NULL, NULL, NULL, 1, 4, '2021-07-07 19:58:25', '2021-07-07 19:58:24', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412742935530827778', '1412617940552937474', 2, 'customer:keep:import', NULL, NULL, NULL, 'CustomerKeepImport', '导入客户', NULL, NULL, NULL, 1, 5, '2021-07-07 19:58:58', '2021-07-07 19:58:57', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412743114564694018', '1412617766615150593', 2, 'customer:export', NULL, NULL, NULL, 'CustomerExport', '导出客户', NULL, NULL, NULL, 1, 8, '2021-07-07 19:59:41', '2021-07-07 19:59:40', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413417385173872642', '1412617504391458817', 1, 'customer:dynamic', '/admin/customer/dynamic', 'admin/customer/dynamic', NULL, 'CustomerDynamic', '客户动态', NULL, NULL, NULL, NULL, 9, '2021-07-09 16:38:59', '2021-07-09 16:38:59', '10001', '', 0);
INSERT INTO `sys_menu` (`id`, `parent_id`, `menu_type`, `permission_tag`, `path`, `component`, `redirect`, `name`, `meta_title`, `meta_icon`, `meta_active_menu`, `meta_no_cache`, `hidden`, `sort`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413426171104124930', '1412617504391458817', 1, 'customer:history', '/admin/customer/history', 'admin/customer/history', NULL, 'CustomerHistory', '客户历史', NULL, NULL, NULL, NULL, 10, '2021-07-09 17:13:54', '2021-07-09 17:13:54', '10001', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` varchar(32) NOT NULL COMMENT '角色ID',
  `role_name` varchar(255) NOT NULL DEFAULT '' COMMENT '角色名称',
  `role_type` int NOT NULL COMMENT '1学员2管理员',
  `data_scope` int NOT NULL COMMENT '数据权限',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注信息',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(255) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(255) NOT NULL DEFAULT '' COMMENT '修改人',
  `data_flag` int NOT NULL DEFAULT '0' COMMENT '数据标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` (`id`, `role_name`, `role_type`, `data_scope`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('employee', '员工', 2, 1, NULL, '2021-07-09 13:34:54', '2021-07-09 13:34:54', '10001', '', 0);
INSERT INTO `sys_role` (`id`, `role_name`, `role_type`, `data_scope`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('sa', '超级管理员', 2, 4, '', '2020-12-03 16:52:16', '2021-07-07 10:20:23', '', '10001', 0);
INSERT INTO `sys_role` (`id`, `role_name`, `role_type`, `data_scope`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('salesleader', '业务主管', 2, 3, NULL, '2021-07-07 17:10:12', '2021-07-07 17:10:11', '10001', '', 0);
INSERT INTO `sys_role` (`id`, `role_name`, `role_type`, `data_scope`, `remark`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('salesman', '业务员', 2, 1, NULL, '2021-07-07 17:09:35', '2021-07-07 17:09:35', '10001', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_menu`;
CREATE TABLE `sys_role_menu` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `role_id` varchar(32) NOT NULL COMMENT '角色ID',
  `menu_id` varchar(32) NOT NULL COMMENT '菜单ID',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(255) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(255) NOT NULL DEFAULT '' COMMENT '修改人',
  `data_flag` int NOT NULL DEFAULT '0' COMMENT '数据标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色菜单授权';

-- ----------------------------
-- Records of sys_role_menu
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413371133207945217', 'employee', '1367010529427607568', '2021-07-09 13:35:11', '2021-07-09 13:35:11', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413371133237305346', 'employee', '1367010529427607569', '2021-07-09 13:35:11', '2021-07-09 13:35:11', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413371133245693953', 'employee', '1412741211327950850', '2021-07-09 13:35:11', '2021-07-09 13:35:11', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235130540033', 'sa', '1367010529427607568', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235143122946', 'sa', '1367010529427607569', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235147317249', 'sa', '1412617504391458817', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235147317250', 'sa', '1412617766615150593', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235151511554', 'sa', '1412656811625988097', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235155705857', 'sa', '1412656531006078978', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235155705858', 'sa', '1412739504917311490', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235159900162', 'sa', '1412740598330421249', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235164094465', 'sa', '1412741211327950850', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235164094466', 'sa', '1412743114564694018', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235168288770', 'sa', '1412617940552937474', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235172483074', 'sa', '1412741344283193345', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235176677377', 'sa', '1412742460639145985', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235176677378', 'sa', '1412741877110796290', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235180871682', 'sa', '1412742235132391426', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235185065986', 'sa', '1412742797139767298', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235189260289', 'sa', '1412742935530827778', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235193454593', 'sa', '1413417385173872642', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235197648897', 'sa', '1413426171104124930', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235197648898', 'sa', '1367010529435996174', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235201843202', 'sa', '1367010529435996175', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235201843203', 'sa', '1399263684152385537', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235206037506', 'sa', '1367010529435996176', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235206037507', 'sa', '1399266644605394945', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235210231809', 'sa', '1399266713060630529', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235210231810', 'sa', '1399266787438223361', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235214426113', 'sa', '1399266868300210177', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235214426114', 'sa', '1367010529435996178', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235218620417', 'sa', '1399266973875036161', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235218620418', 'sa', '1399267041344610306', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235218620419', 'sa', '1399267166364229634', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235222814722', 'sa', '1399270377145884673', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235222814723', 'sa', '1367010529435996177', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235227009025', 'sa', '1399270608335921153', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235227009026', 'sa', '1399270698039500801', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235231203329', 'sa', '1399270777932603393', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235231203330', 'sa', '1367010529435996179', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235235397634', 'sa', '1399271425583472641', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235235397635', 'sa', '1399271482458234882', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235239591938', 'sa', '1399271543669907458', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235243786241', 'sa', '1399271602981560321', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235243786242', 'sa', '1399271669553553410', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235252174850', 'sa', '1367010529435996180', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235256369154', 'sa', '1399273800431951873', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235260563458', 'sa', '1367010529435996183', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235260563459', 'sa', '1399272795329912833', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235264757761', 'sa', '1399272872786124802', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235268952066', 'sa', '1399272937902694402', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235268952067', 'sa', '1399273058870616065', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235273146369', 'sa', '1367010529435996184', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235273146370', 'sa', '1399273168916570113', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235277340673', 'sa', '1399273214881947650', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235277340674', 'sa', '1399273324386836482', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413430235281534978', 'sa', '1399273388576464897', '2021-07-09 17:30:03', '2021-07-09 17:30:03', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726294769666', 'salesman', '1367010529427607568', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726307352577', 'salesman', '1367010529427607569', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726307352578', 'salesman', '1412617504391458817', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726311546881', 'salesman', '1412617766615150593', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726311546882', 'salesman', '1412741211327950850', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726315741185', 'salesman', '1412617940552937474', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726315741186', 'salesman', '1412741344283193345', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726319935490', 'salesman', '1412742460639145985', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726319935491', 'salesman', '1412741877110796290', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726324129793', 'salesman', '1412742235132391426', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726324129794', 'salesman', '1412742797139767298', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726328324097', 'salesman', '1412742935530827778', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726328324098', 'salesman', '1413417385173872642', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434726332518402', 'salesman', '1413426171104124930', '2021-07-09 17:47:53', '2021-07-09 17:47:53', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805466451970', 'salesleader', '1367010529427607568', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805474840577', 'salesleader', '1367010529427607569', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805479034881', 'salesleader', '1412617504391458817', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805479034882', 'salesleader', '1412617766615150593', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805483229186', 'salesleader', '1412656811625988097', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805483229187', 'salesleader', '1412656531006078978', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805487423489', 'salesleader', '1412741211327950850', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805487423490', 'salesleader', '1412617940552937474', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805487423491', 'salesleader', '1412741344283193345', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805491617793', 'salesleader', '1412742460639145985', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805491617794', 'salesleader', '1412741877110796290', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805491617795', 'salesleader', '1412742235132391426', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805495812098', 'salesleader', '1412742797139767298', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805495812099', 'salesleader', '1412742935530827778', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805495812100', 'salesleader', '1413417385173872642', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
INSERT INTO `sys_role_menu` (`id`, `role_id`, `menu_id`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413434805500006401', 'salesleader', '1413426171104124930', '2021-07-09 17:48:12', '2021-07-09 17:48:12', '', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_user`;
CREATE TABLE `sys_user` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `user_name` varchar(255) NOT NULL DEFAULT '' COMMENT '用户名',
  `avatar` varchar(255) NOT NULL DEFAULT '' COMMENT '头像',
  `real_name` varchar(255) NOT NULL DEFAULT '' COMMENT '真实姓名',
  `password` varchar(255) NOT NULL DEFAULT '' COMMENT '密码',
  `salt` varchar(255) NOT NULL DEFAULT '' COMMENT '密码盐',
  `state` int NOT NULL DEFAULT '0' COMMENT '状态',
  `points` int NOT NULL DEFAULT '0' COMMENT '积分',
  `id_card` varchar(255) DEFAULT '' COMMENT '身份证号码',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机号',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `dept_code` varchar(32) DEFAULT NULL COMMENT '部门编码',
  `create_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '更新时间',
  `create_by` varchar(255) NOT NULL DEFAULT '' COMMENT '创建人',
  `update_by` varchar(255) NOT NULL DEFAULT '' COMMENT '修改人',
  `data_flag` int NOT NULL DEFAULT '0' COMMENT '数据标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='管理用户';

-- ----------------------------
-- Records of sys_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_user` (`id`, `user_name`, `avatar`, `real_name`, `password`, `salt`, `state`, `points`, `id_card`, `mobile`, `email`, `dept_code`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('10001', 'admin', 'https://face-files.oss-cn-shenzhen.aliyuncs.com/logo.png', '超管', '06681cd08837b21adf6b5ef9279d403d', 'XoFFuS', 0, 138, '', NULL, NULL, 'A01', '2020-12-03 16:52:10', '2021-07-09 19:44:39', '', '10001', 1);
INSERT INTO `sys_user` (`id`, `user_name`, `avatar`, `real_name`, `password`, `salt`, `state`, `points`, `id_card`, `mobile`, `email`, `dept_code`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412700694430924802', 'man1', '', '张大保', '', 'lJMmIW', 0, 0, '', NULL, '18365918@qq.com', 'A01A01', '2021-07-07 17:11:07', '2021-07-09 19:44:21', '10001', '10001', 0);
INSERT INTO `sys_user` (`id`, `user_name`, `avatar`, `real_name`, `password`, `salt`, `state`, `points`, `id_card`, `mobile`, `email`, `dept_code`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412700925297999873', 'leader', '', '王经理', '', 'cZAoiI', 0, 0, '', NULL, '835487894@qq.com', 'A01A01', '2021-07-07 17:12:02', '2021-07-09 19:44:30', '10001', '10001', 0);
INSERT INTO `sys_user` (`id`, `user_name`, `avatar`, `real_name`, `password`, `salt`, `state`, `points`, `id_card`, `mobile`, `email`, `dept_code`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1412713579886460930', 'man2', '', '郭小花', '', 'eNbtOL', 0, 0, '', NULL, NULL, 'A01A01A02', '2021-07-07 18:02:19', '2021-07-09 19:44:07', '10001', '10001', 0);
INSERT INTO `sys_user` (`id`, `user_name`, `avatar`, `real_name`, `password`, `salt`, `state`, `points`, `id_card`, `mobile`, `email`, `dept_code`, `create_time`, `update_time`, `create_by`, `update_by`, `data_flag`) VALUES ('1413400517818163202', '18682216559', '', '郭银子', '92cc85a910c33687fa608150ce1fc3d2', 'iqgmWo', 0, 0, '', '18682216559', NULL, 'A01A01A01', '2021-07-09 15:31:58', '2021-07-09 15:31:57', '', '', 0);
COMMIT;

-- ----------------------------
-- Table structure for sys_user_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_user_role`;
CREATE TABLE `sys_user_role` (
  `id` varchar(32) NOT NULL COMMENT 'ID',
  `user_id` varchar(32) NOT NULL COMMENT '用户ID',
  `role_id` varchar(32) NOT NULL COMMENT '角色ID',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户角色';

-- ----------------------------
-- Records of sys_user_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES ('1413372964627337217', '1413372964312764417', 'employee');
INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES ('1413392966893981697', '1413392966780735490', 'employee');
INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES ('1413400518006906882', '1413400517818163202', 'employee');
INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES ('1413463974668046338', '1412713579886460930', 'salesman');
INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES ('1413464032843042818', '1412700694430924802', 'salesman');
INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES ('1413464071854264322', '1412700925297999873', 'salesleader');
INSERT INTO `sys_user_role` (`id`, `user_id`, `role_id`) VALUES ('1413464108369874946', '10001', 'sa');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
